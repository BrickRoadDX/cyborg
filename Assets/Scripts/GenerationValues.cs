﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerationValues : MonoBehaviour
{

    public static int NumToSpawn(ActorType type)
    {
        switch(type)
        {
            case ActorType.Guard:
                return 5;
        }

        return 1;
    }
}
