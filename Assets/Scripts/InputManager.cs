﻿using Gamelogic.Grids;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : Singleton<InputManager>
{
    public Actor castingStartSelectedUnit;

    public Actor selectedUnit;
    public SpriteRendererGo selectedUnitVisual;

    public void SelectCell(RectCell cell)
    {
        switch(Game.Instance.inputState)
        {
            //case InputState.Playing:
            //    selectedUnit.actorPath.TryAddToPath(cell);
            //    return;

            case InputState.CastingCard:
                if(cell.HasPlayerUnit())
                {
                    SetSelectedUnit(cell.actor, false);
                    ShowCardMoves();
                    return;
                }
                if(Board.Instance.cellHighlightCells.Contains(cell))
                {
                    CardManager.Instance.DoneCastingCard();
                    DoMove(cell);
                    return;
                }

                break;
        }
    }

    void DoMove(RectCell newCell)
    {
        Actor toMove = selectedUnit;
        UnsetSelectedUnit();
        toMove.Move(newCell.point);
        Game.Instance.EnemyTurn();
    }

    void TryArrowMove(RectPoint dir)
    {
        RectPoint newPoint = selectedUnit.cell.point + dir;
        if(newPoint.HasCell())
        {
            DoMove(newPoint.ToCell());
        }
    }

    private void Update()
    {
        if (Helpers.GameActionsBlocked())
            return;

        switch(Game.Instance.inputState)
        {
            case InputState.Playing:
                if (Input.GetKeyUp(KeyCode.UpArrow))
                {
                    TryArrowMove(RectPoint.North);
                    return;
                }
                else if (Input.GetKeyUp(KeyCode.RightArrow))
                {
                    TryArrowMove(RectPoint.East);
                    return;
                }
                else if (Input.GetKeyUp(KeyCode.LeftArrow))
                {
                    TryArrowMove(RectPoint.West);
                    return;
                }
                else if (Input.GetKeyUp(KeyCode.DownArrow))
                {
                    TryArrowMove(RectPoint.South);
                    return;
                }
                break;
        }
    }

    public void Init()
    {
        if(selectedUnitVisual == null)
        {
            selectedUnitVisual = Helpers.SpawnSpriteRenderer("box-select", this.transform, Color.white, 10);
            PingPongScaler scaler = selectedUnitVisual.gameObject.AddComponent<PingPongScaler>();
        }
    }

    public void UnsetSelectedUnit()
    {
        selectedUnitVisual.transform.parent = this.transform;
        selectedUnit = null;
        selectedUnitVisual.gameObject.SetActive(false);
    }

    List<Actor> upcomingUnits = new List<Actor>();
    public void NextSelectedUnit()
    {
        upcomingUnits.RemoveAll(x => !Board.Instance.GetPlayerUnits().Contains(x));
        if(upcomingUnits.Count < 1)
        {
            upcomingUnits = Board.Instance.GetPlayerUnits();
        }

        SetSelectedUnit(upcomingUnits.PopFirst());
    }

    void SetSelectedUnit(Actor newUnit, bool startUnit = true)
    {
        selectedUnit = newUnit;
        selectedUnitVisual.transform.parent = selectedUnit.transform;
        selectedUnitVisual.transform.localPosition = Vector3.zero;
        selectedUnitVisual.transform.localScale = Vector3.one;
        selectedUnitVisual.gameObject.SetActive(true);

        if (startUnit)
            castingStartSelectedUnit = selectedUnit;
    }

    internal void ShowCardMoves()
    {
        CardPattern pattern = CardManager.Instance.castingCardObject.card.GetCardPattern();
        if (!pattern.HasSpaceWithType(EffectType.Start))
            return;

        Board.Instance.RemoveCellHighlights();

        List<RectCell> moves = new List<RectCell>();

        RectCell origin = selectedUnit.cell;
 
        PatternSpace start = pattern.GetSpaceWithType(EffectType.Start);
        PatternSpace end = pattern.GetSpaceWithType(EffectType.End);

        RectPoint diff = end.position - start.position;

        foreach(Direction d in Helpers.MainDirections())
        {
            RectPoint p = Helpers.GetRotatedRectPoint(diff, d);
            RectPoint newPoint = origin.point + p;
            if (newPoint.HasCell())
                moves.Add(newPoint.ToCell());
        }
        diff = diff.ReflectAboutX();
        foreach (Direction d in Helpers.MainDirections())
        {
            RectPoint p = Helpers.GetRotatedRectPoint(diff, d);
            RectPoint newPoint = origin.point + p;
            if (newPoint.HasCell())
                moves.Add(newPoint.ToCell());
        }

        Board.Instance.HighlightCells(moves);

    }

    internal void ResetSelectedUnit()
    {
        SetSelectedUnit(castingStartSelectedUnit);
    }
}
