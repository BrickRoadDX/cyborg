﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TerrainType
{
    None = 0,

    Hole = 1,
    Gate = 2,

}

public class RectCell : RectCellBase
{
    public void SetTerrainNone()
    {
        SetTerrainType(TerrainType.None);
    }

    public new void SetTerrainType(TerrainType type)
    {
        this.terrainType = type;

        switch (type)
        {
            case TerrainType.None:
                terrainSprite.gameObject.SetActive(false);
                backgroundSprite.color = startingColorCache;
                break;
            case TerrainType.Gate:
                terrainSprite.gameObject.SetActive(true);
                terrainSprite.sprite = Helpers.GetSprite("doorway");
                terrainSprite.color = Constants.axesRoad;
                break;
        }
    }
}
