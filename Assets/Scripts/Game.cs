﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public enum InputState
{
    None = 0,
    Playing = 1,
    EnemyTurn = 2,
    CastingCard = 3,

    Ending = 10,
}

public class Game : Singleton<Game> {

    public Camera mainCamera;
    public Camera tk2dCamera;
    public Transform cameraHolder;

    public InputState inputState = InputState.None;

    public CardType showcaseCardOverride = CardType.None;


    private void Start()
    {
        NewGame();
    }

    void StartTurn()
    {
        InputManager.Instance.NextSelectedUnit();
    }

    private void OnEnable()
    {

    }

    internal void NewGame()
    {
        Board.Instance.Init();
        ResourceManager.Instance.Init();
        CardManager.Instance.Init();
        InputManager.Instance.Init();
        GoToBaseState();

        StartTurn();
    }

    public bool CanAfford(Player player, Card card)
    {
        return ResourceManager.Instance.GetValue(ResourceType.Money) >= Card.GetCost(card.cardType);
    }

    internal void BuyCard(Player player, CardObject cardObject)
    {
        //AdjustMomentum(player, -Card.GetMomentumCost(cardObject.card.type));
        //cardMarket.RemoveObject(cardObject);

        cardObject.transform.localScale = Vector3.one * 0.75f;
        //display.AddObject(cardObject);
    }

    internal void GoToBaseState()
    {
        GoToState(InputState.Playing);
    }

    public void GoToState(InputState state)
    {
        inputState = state;

        Board.Instance.RemoveCellHighlights();

        switch(state)
        {
            case InputState.CastingCard:
                InputManager.Instance.ShowCardMoves();
                break;
        }
    }

    internal bool IsGameOver()
    {
        return false;
    }

    private void Update()
    {
#if UNITY_EDITOR
        DebugInput();
#endif
    }

    void DebugInput()
    {
        if (Input.GetKeyUp(KeyCode.D))
            CardManager.Instance.DrawCard();

        if (Input.GetKeyUp(KeyCode.S))
            Helpers.RotateGameSpeed();

        if (Input.GetKeyUp(KeyCode.N))
            NewGame();
    }

    void AdjustTurnsRemaining(int amount)
    {
        ResourceManager.Instance.AdjustValue(ResourceType.Turns, amount);
    }

    bool preMovesDone = true;
    bool isEnemyTurn = false;
    Sequence enemyTurn;
    List<Actor> enemiesToMove = null;
    List<Actor> preMoveActors = null;
    public void EnemyTurn()
    {
        if (isEnemyTurn)
            return;

        if (IsGameOver() || inputState == InputState.Ending)
            return;

        preMovesDone = false;

        AdjustTurnsRemaining(-1);

        if (ResourceManager.Instance.GetValue(ResourceType.Turns) == 0)
        {
            GameOver(false, "You failed to clear out the ruffians before reinforcements arrived. Retreat!");
            return;
        }

        GoToState(InputState.EnemyTurn);

        enemyTurn = DOTween.Sequence();

        enemyTurn.AppendInterval(Constants.ENEMY_MOVE_TIME);

        foreach (Actor a in Board.Instance.GetEnemyUnits())
        {
            if (a.HasPreMove())
            {
                PreMove(a);
            }
        }

        foreach (Actor a in Board.Instance.GetEnemyUnits())
            a.CacheEnemyMoves();

        enemiesToMove = Board.Instance.GetEnemyUnits();

        enemiesToMove.Sort((Actor x, Actor y) => ((int)(y.type) - (int)(x.type))); //takes an int

        foreach (Actor actor in enemiesToMove)
            MoveEnemy(actor);

        enemyTurn.AppendInterval(0.1f);

        enemyTurn.AppendCallback(EnemyTurnOver);
        if (!IsGameOver())
            enemyTurn.Play();
    }

    void PreMove(Actor actor)
    {
        enemyTurn.AppendCallback(delegate () {
            if (!actor.dying)
            {
                enemyTurn.Pause();
                actor.DoPreMove(UnpauseEnemyTurn);
            }
        });   
    }

    public void PauseEnemyTurn()
    {
        if (enemyTurn != null)
            enemyTurn.Pause();
    }

    void UnpauseEnemyTurn()
    {
        if (enemyTurn != null)
            enemyTurn.Play();
    }

    void MoveEnemy(Actor actor)
    {
        enemyTurn.AppendCallback(delegate ()
        {
            actor.cell.ShowColorAnimation(Color.white * 0.6f, 1f);
        });

        enemyTurn.AppendInterval(Constants.ENEMY_MOVE_TIME);

        enemyTurn.AppendCallback(delegate () {
            if (!enemiesToMove.First().dying)
            {
                actor.DoEnemyMove();
            }
            enemiesToMove.RemoveAt(0);
        });
    }

    void EnemyTurnOver()
    {
        Board.Instance.cacheAllEnemyMoves = true;

        GoToState(InputState.Playing);

        StartTurn();
    }

    private bool mGameOver = false;
    public void GameOver(bool playerWon, string gameOverText = "Game Over!", bool abandon = false)
    {
        if (mGameOver)
            return;

        mGameOver = true;

        PopupManager.Instance.ClearQueue();
        PopupManager.Instance.DismissPopup();

        Board.Instance.RemoveCellHighlights();

        PopupManager.Instance.ShowPopupOneButton("Game Over", "New Game", NewGame);
    }

    internal void UndidCard()
    {
        InputManager.Instance.ResetSelectedUnit();
        GoToBaseState();
    }
}
