﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ticks : MonoBehaviour
{
    public Transform tickContainer;
    public string tickPrefabName;
    public int numTicks;
    public float tickDistance;

    void Start()
    {
        float yPos = tickDistance;

        for(int i = 0; i < numTicks; i++)
        {
            GameObject tick = Helpers.Create(tickPrefabName);
            tick.transform.parent = tickContainer;
            tick.transform.localPosition = Vector3.zero;
            tick.transform.SetLocalPositionX(-1f);
            tick.transform.SetLocalPositionY(yPos);

            if((i+1) % 5 != 0)
                tick.transform.localScale = new Vector3(0.5f, 1f, 1f);

            tick.transform.SetLocalScaleY(0.5f);

            yPos += tickDistance;
        }
    }

}
