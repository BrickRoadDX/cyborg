﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cooldown : MonoBehaviour
{
    public delegate bool BoolFunction();

    float timespan; //how long the cooldown is
    public Action callback; //callback for when the timer hits zero
    public BoolFunction updateCheckFunction; //timer will only tick if this function evaluates to true

    float timer = 0f;

    public bool paused = true;

    private Cooldown(bool thisDoesntWorkUseHelperMethod)
    {

    }

    private void Update()
    {
        if (updateCheckFunction != null && !updateCheckFunction())
            return;

        if(!IsReady() && !paused)
        {
            timer -= Time.deltaTime;

            if (callback != null && IsReady())
                callback();
        }
    }

    internal void Pause()
    {
        paused = true;
    }

    public void Play()
    {
        paused = false;
    }

    public bool IsReady()
    {
        return timer <= 0f;
    }

    public void Reset()
    {
        timer = timespan;
    }

    public void StartCooldown(float newDuration, bool preFinishCooldown = false, bool startPaused = false)
    {
        this.timespan = newDuration;
        paused = startPaused;
        Reset();

        if (preFinishCooldown)
            timer = 0f;
    }

    public float GetTimespan()
    {
        return timespan;
    }
}
