﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using System;

public class TooltipObject : MonoBehaviour {

	public TextMeshPro tooltipText;

	public int width;
	public int height;
    public float xOffset = 0f;
    public float yOffset = 0f;

    internal void SetPositionFromWorldPosition(Vector3 position)
    {
        Vector3 pos = Camera.main.WorldToScreenPoint(position);
        this.transform.position = Game.Instance.tk2dCamera.ScreenToWorldPoint(pos);

        if (Game.Instance.tk2dCamera.ScreenToViewportPoint(pos).x > 0.5f)
        {
            transform.SetPositionX(transform.position.x - width - xOffset);
        }
        else
        {
            transform.SetPositionX(transform.position.x + xOffset);
        }

        if (Game.Instance.tk2dCamera.ScreenToViewportPoint(pos).y > 0.5f)
        {
            transform.SetPositionY(transform.position.y - height - yOffset);
        }
        else
        {
            transform.SetPositionY(transform.position.y + yOffset);
        }
    }
}
