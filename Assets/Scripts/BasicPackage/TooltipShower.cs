﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum TooltipType
{
    None = 0,
}

public class TooltipShower : MonoBehaviour
{
	//Statics
	public static TooltipObject currentTooltip = null;
	public static int currentShownId = -1;
	public static float currentHoverDuration = 0f;
	public static int nextShowerId = 0;
	//End Statics


	//Settings
	public tk2dUIItem hoverUiItem;
	public float hoverDelay = 0.3f;

	private int mThisTooltipId = -1;

	public string textToShow = "";

    public TooltipType tooltipType = TooltipType.None;
    public string prefabName = "TooltipObject";

    [HideInInspector]
    public Camera tk2dCamera;

	void OnEnable()
	{
        if(tk2dCamera == null)
            tk2dCamera = FindObjectOfType<tk2dCamera>().GetComponent<Camera>();

        hoverUiItem.isHoverEnabled = true;
		hoverUiItem.OnHoverOver += HandleHoverOver;
		hoverUiItem.OnHoverOut += HandleHoverOut;
		mThisTooltipId = nextShowerId;
		nextShowerId++;
    }

	void OnDisable()
	{
		hoverUiItem.OnHoverOver -= HandleHoverOver;
		hoverUiItem.OnHoverOver += HandleHoverOut;
	}

	void LateUpdate()
	{
		if(mThisTooltipId == currentShownId)
		{
			currentHoverDuration += Time.deltaTime;

			if(currentHoverDuration > hoverDelay)
			{
				ShowTooltip();
			}
		}
	}

	void HandleHoverOver()
	{
		DestroyCurrentTooltip();
		currentShownId = mThisTooltipId;
	}

	protected virtual TooltipObject ShowTooltip()
	{
        if (Helpers.IsGameScene())
            tk2dCamera = Game.Instance.tk2dCamera;

        if (currentTooltip == null)
		{
            if(textToShow.Length > 0)
            {
                currentTooltip = Helpers.CreateInstance<TooltipObject>(prefabName, this.transform);
                currentTooltip.transform.position = tk2dCamera.ScreenToWorldPoint(Input.mousePosition) + new Vector3(0f, -currentTooltip.height, 0f);
                if (tk2dCamera.ScreenToViewportPoint(Input.mousePosition).x > 0.5f)
                {
                    currentTooltip.transform.SetPositionX(currentTooltip.transform.position.x - currentTooltip.width + currentTooltip.xOffset);
                }

                if (tk2dCamera.ScreenToViewportPoint(Input.mousePosition).y < 0.5f)
                {
                    currentTooltip.transform.SetPositionY(currentTooltip.transform.position.y + currentTooltip.height + currentTooltip.yOffset);
                }

                currentShownId = mThisTooltipId;

                currentTooltip.tooltipText.text = textToShow;
            }
        }

        return currentTooltip;
	}

	void HandleHoverOut()
	{
		if(currentShownId == mThisTooltipId)
		{
			DestroyCurrentTooltip();
		}
	}

	public static void DestroyCurrentTooltip()
	{
		if (currentTooltip != null)
		{
			Destroy(currentTooltip.gameObject);
		}

		currentHoverDuration = 0f;
		currentTooltip = null;
		currentShownId = -1;
	}
}
