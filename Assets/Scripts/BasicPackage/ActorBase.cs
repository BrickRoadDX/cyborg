﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Gamelogic.Grids;
using System;
using DG.Tweening;
using System.Linq;
using TMPro;
using UnityEngine.UI;

public enum DeathAnimation
{
    None = 0,
    Fly = 1,
    Fall = 2,
    PositionFall = 3,
    CustomDeathAnim = 4,
}

public class ActorBase : MonoBehaviour
{
    public static int nextActorId = 1;

    [HideInInspector]
    public int actorId = 0;

    public RectGrid<RectCell> grid;

    [HideInInspector]
    public RectCell cell;

    [HideInInspector]
    public ActorType type;

    public TextMeshPro textMesh;
    public SpriteRenderer spriteRenderer;

    [HideInInspector]
    public bool dying = false;

    [HideInInspector]
    public RectCell previousPosition;

    [HideInInspector]
    public Direction movedDirection = Direction.None;

    public virtual void Init(ActorType aType)
    {
        grid = Board.Instance.grid;
        textMesh.text = "";

        ((Actor)this).Init();
    }

    internal void Attack(Actor attacker)
    {
        bool playerCaused = false;

        if (attacker != null)
        {
            if (attacker.IsPlayerUnit())
                playerCaused = true;
        }

        cell.DestroyActor(DeathAnimation.Fly, Board.Instance.map[attacker.cell.point + Helpers.GetRectPointFromDirection(attacker.movedDirection)] - Board.Instance.map[attacker.cell.point]);
    }

    public void HandleActorKilled()
    {
        dying = true;

        Board.Instance.cacheAllEnemyMoves = true;
    }

    void SetMovedDirection(RectPoint offset)
    {
        if (offset.X > 0)
        {
            SetMovedDirection(Direction.Right);
        }

        if (offset.X < 0)
        {
            SetMovedDirection(Direction.Left);
        }

        if (offset.Y > 0)
        {
            SetMovedDirection(Direction.Up);
        }

        if (offset.Y < 0)
        {
            SetMovedDirection(Direction.Down);
        }
    }

    void SetMovedDirection(Direction dir)
    {
        movedDirection = dir;
    }

    public void Move(RectPoint newPoint)
    {
        if (grid.Contains(newPoint))
        {
            Move(cell, grid[newPoint]);
        }
    }

    public void Move(RectCell oldCell, RectCell newCell)
    {
        if (newCell == oldCell)
            return;

        SetMovedDirection(newCell.point - oldCell.point);

        Actor actorToAttack;

        actorToAttack = null;
        if (newCell.HasLivingActor())
            actorToAttack = newCell.actor;

        bool attacked = false;

        if (actorToAttack != null)
        {
            actorToAttack.Attack((Actor)this);
            attacked = true;

            if (actorToAttack != null)
                actorToAttack.cell.DestroyActor(DeathAnimation.Fly);
        }
        else
        {
            if (this.IsPlayerUnit())
                SoundManager.Instance.PlaySound("step");
        }

        if (newCell.HasLivingActor())
            return;

        if (oldCell.actor == this)
            oldCell.RemoveActor();

        previousPosition = cell;

        cell = newCell;
        newCell.AddActor((Actor)this);

        if (this.IsPlayerUnit())
            Board.Instance.cacheAllEnemyMoves = true;

        HandleMoveAnimation(newCell);

        ((Actor)this).HandleMoved();
    }

    internal void HandleMoveAnimation(RectCell rectCell)
    {
        this.transform.DOKill();
        this.transform.DOLocalMove(Vector3.zero, 0.19f);
    }

    public bool IsEnemy()
    {
        return Actor.IsEnemy(type);
    }

    public bool IsPlayerUnit()
    {
        return Actor.IsPlayerUnit(type);
    }

    public void EscapeAnim()
    {
        float duration = 1f;
        this.transform.DORotate(new Vector3(0f, 0f, 360f), duration, RotateMode.LocalAxisAdd).Play();
        this.transform.DOScale(2f, duration).Play();

        Helpers.MakeTemporary(this.gameObject, duration);
    }

}
