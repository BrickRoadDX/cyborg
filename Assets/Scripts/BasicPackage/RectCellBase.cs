﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Gamelogic.Grids;
using System;
using DG.Tweening;
using System.Linq;
using TMPro;

public class RectCellBase : SpriteCell
{
    public TextMeshPro debugText;
    public Transform actorContainer;

    [HideInInspector]
    public Actor actor;

    public RectGrid<RectCell> grid;
    [HideInInspector]
    public RectPoint point;
    public bool isOccupied { get { return actor != null; } }

    [HideInInspector]
    public TerrainType terrainType = TerrainType.None;
    public SpriteRenderer backgroundSprite;
    public SpriteRenderer terrainSprite;

    private void Start()
    {
        debugText.text = "";
    }

    public void SetDebugText(string text)
    {
        debugText.text = text;
    }

    public void Init()
    {
        startingColorCache = Color;
        SetColor(startingColorCache);

        SetDebugText("");
        ((RectCell)this).SetTerrainNone();

        this.StopDelayedActions();

        actor = null;
    }

    public Actor SpawnActor(ActorType type, bool wasKilled = true, bool animation = false)
    {
        if (type == ActorType.None)
            return null;

        if (actor != null)
            DestroyActor(DeathAnimation.Fly, Vector3.zero, wasKilled);

        Actor a = Helpers.CreateInstance<Actor>("Actor", this.transform);

        if(animation)
        {
            a.transform.localScale = Vector3.one * 0.1f;
            a.transform.DOScale(1f, 0.3f);
        }

        a.transform.parent = actorContainer;
        a.spriteRenderer.color = Actor.GetActorColor(type);

        a.type = type;
        AddActor(a);
        a.Init(a.type);

        a.transform.localPosition = Vector3.zero;

        Board.Instance.actorsOnBoard.Add(a);

        a.actorId = Actor.nextActorId;
        Actor.nextActorId++;
        a.gameObject.name = a.type.ToString() + " " + a.actorId.ToString();

        Sprite s = Helpers.GetSprite(Actor.GetSpriteName(type));

        a.spriteRenderer.sprite = s;

        return a;
    }

    public void AddActor(Actor a)
    {
        this.actor = a;
        a.cell = (RectCell)this;
        a.transform.parent = this.actorContainer;
    }

    public void RemoveActor()
    {
        actor = null;
    }

    public void SetColor(Color c)
    {
        backgroundSprite.color = c;
    }

    internal void HoverOut()
    {

    }


    internal void HoverOver()
    {

    }

    public void Init(RectGrid<RectCell> grid, RectPoint point)
    {
        this.grid = grid;
        this.point = point;
        this.name = point.ToString();
    }

    internal bool IsWalkable()
    {
        return TerrainGroundWalkable(terrainType);
    }

    internal bool IsPlayerWalkable()
    {
        return IsWalkable() && !isOccupied;
    }

    internal bool IsEnemyWalkable()
    {
        return IsWalkable() && !HasEnemy();
    }

    internal bool HasAdjacentActor(ActorType type)
    {
        foreach (RectCell cell in Board.Instance.GetAdjacent((RectCell)this))
        {
            if (cell.GetActorType() == type)
                return true;
        }
        return false;
    }

    internal bool IsPushable()
    {
        return true;
    }

    internal bool HasCellVisual()
    {
        return Board.Instance.cellHighlightCells.Contains(this);
    }

    internal bool HasEnemy()
    {
        return Actor.IsEnemy(GetActorType());
    }

    internal bool IsTall()
    {
        return isOccupied;
    }

    internal bool HasPlayerUnit()
    {
        return Actor.IsPlayerUnit(GetActorType());
    }

    public static int counter = 0;
    internal void Attack(Actor attacker)
    {
        ShowColorAnimation(Color.red * 0.75f, 1f);

        if (actor != null)
        {
            actor.Attack(attacker);
        }
    }

    public void DestroyActor(DeathAnimation deathAnimType = DeathAnimation.None, Vector3 direction = default(Vector3), bool wasKilled = true)
    {
        if (actor == null)
            return;

        Actor a = actor;

        if (Board.Instance.actorsOnBoard.Contains(actor))
            Board.Instance.actorsOnBoard.Remove(actor);

        RemoveActor();

        float deathTime = 0f;

        switch (deathAnimType)
        {
            case DeathAnimation.None:
                a.gameObject.DestroySelf();
                break;

            case DeathAnimation.Fly:
                float tweenDuration = 4f;
                Vector3 pos = Helpers.CirclePoint(Helpers.RandomFloatFromRangeInclusive(0f, 1f), Board.Instance.GetBoardWidth() * 4f, a.transform.position);

                if (direction != default(Vector3) && direction != Vector3.zero)
                {
                    direction = direction.normalized;
                    pos = direction * Board.Instance.GetBoardWidth() * 7.5f;
                }

                a.transform.DOMove(pos, tweenDuration).SetEase(Ease.Linear).OnComplete(delegate () { Destroy(a.gameObject); });
                a.transform.DOScale(Vector3.one * 25f, tweenDuration);
                deathTime = 1f;
                break;
            case DeathAnimation.Fall:
                a.transform.DORotate(new Vector3(0f, 0f, 45f), 0.3f);
                a.transform.DOScale(0.01f, 2f);
                a.gameObject.AddComponent<Temporary>().mLifespan = 1f;
                deathTime = 1f;
                SoundManager.Instance.PlaySound("fall");
                this.CallActionDelayed(delegate ()
                {
                    SoundManager.Instance.PlaySound("splash");
                }, deathTime);

                break;
            case DeathAnimation.PositionFall:
                a.transform.DOMove(direction, 0.3f);
                a.transform.DORotate(new Vector3(0f, 0f, 45f), 0.3f);
                a.transform.DOScale(0.01f, 2f);
                a.gameObject.AddComponent<Temporary>().mLifespan = 1f;
                deathTime = 1f;
                SoundManager.Instance.PlaySound("fall");

                this.CallActionDelayed(delegate ()
                {
                    SpriteEffect spriteEffect = Helpers.SpawnSpriteEffect("Splash", this.transform);
                    spriteEffect.gameObject.transform.position = direction;
                    SoundManager.Instance.PlaySound("splash");
                }, deathTime);
                break;
        }

        if (wasKilled)
        {
            a.HandleActorKilled();
        }
    }

    internal bool HasLivingActor()
    {
        return actor != null;
    }

    internal void DestroyCell()
    {
        if (actor != null)
            DestroyActor(DeathAnimation.None);

        this.gameObject.DestroySelf();
    }

    public ActorType GetActorType()
    {
        if (actor == null)
            return ActorType.None;

        return actor.type;
    }

    public int GetPushDepth(RectPoint dir)
    {
        int count = 0;

        Helpers.StartWhileLoopCounter();

        RectCell currCell = (RectCell)this;

        while (currCell != null && currCell.HasLivingActor())
        {
            if (currCell.HasLivingActor())
                count++;

            currCell = Board.Instance.GetCellIfAvailable(currCell.point + dir);
        }

        return count;
    }

    [HideInInspector]
    public List<RectCell> pushVisited = new List<RectCell>();
    public void Push(RectPoint dir, bool iceSlide = false, bool initialPush = true)
    {
        if (initialPush)
            pushVisited.Clear();

        pushVisited.Add((RectCell)this);

        if (actor == null)
            return;

        if (initialPush)
            SoundManager.Instance.PlaySound("WCShort1");


        RectPoint newPoint = this.point + dir;

        if (!Board.Instance.CellIsAvailalable(newPoint)) // board does not contain this space
        {
            if(actor.IsPlayerUnit())
            {
                newPoint = newPoint.GetReflection();
                newPoint.ToCell().Push(dir, iceSlide, false);

                if(actor != null)
                    actor.Move(newPoint);
                return;
            }

            actor.transform.DOKill();
            actor.transform.DOMove(Board.Instance.map[this.point + dir], 0.3f);
            this.DestroyActor(DeathAnimation.Fall, default(Vector3));            //off edge

            this.CallActionDelayed(delegate ()
            {
                SpriteEffect spriteEffect = Helpers.SpawnSpriteEffect("Splash", this.transform);
                spriteEffect.gameObject.transform.position = Board.Instance.map[this.point + dir];

            }, 1f);
            return;
        }

        RectCell newCell = Board.Instance.grid[this.point + dir];

        if (newCell.terrainType == TerrainType.Hole)
        {
            ActorFallInHole(actor, newCell);
            return;
        }
        else if (!newCell.IsPushable())
        { 
            return; //don't push into doors or walls
        }

        if (newCell.HasLivingActor()) //continue to push next object
        {
            if(!iceSlide)
                newCell.Push(dir, iceSlide, false);
        }

        if (this.actor != null) //continue to slide self
        {
            if (iceSlide)
            {
                if(!newCell.HasLivingActor())
                {
                    this.actor.Move(newCell.point);
                }
            }
            else
            {
                this.actor.Move(newCell.point);
            }

            //if (newCell.terrainType == TerrainType.Ice) //reason to keep sliding
            if(iceSlide)
            {
                newCell.Push(dir, true);
            }
        }
    }

    public static void ActorFallInHole(Actor a, RectCell holeCell)
    {
        if (a.dying)
            return;

        holeCell.actor = a;
        a.transform.DOKill();
        a.transform.DOMove(Board.Instance.map[holeCell.point], 0.3f);
        holeCell.DestroyActor(DeathAnimation.Fall, default(Vector3));
        holeCell.CallActionDelayed(holeCell.Splash, 1f);
        holeCell.CallActionDelayed(holeCell.SetTerrainNone, 1f);
    }

    public void Splash()
    {
        Helpers.SpawnSpriteEffect("Splash", this.transform);
        SoundManager.Instance.PlaySound("splash");
    }

    internal void SpawnSpriteEffect(string spriteName)
    {
        Helpers.SpawnSpriteEffect(spriteName, this.transform);
    }

    public static bool TerrainGroundWalkable(TerrainType type)
    {
        switch(type)
        {
            case TerrainType.Hole:
                return false;
        }

        return true;
    }

    public static List<RectPoint> AddReflections(List<RectPoint> points)
    {
        List<RectPoint> reflections = new List<RectPoint>();
        foreach (RectPoint point in points)
        {
            int x = Helpers.mod(point.X, Board.Instance.grid.Width);
            int y = Helpers.mod(point.Y, Board.Instance.grid.Height);

            reflections.Add(new RectPoint(x, y));
        }

        reflections.RemoveAll(x => !Board.Instance.grid.Contains(x));

        return reflections;
    }

    public static List<RectPoint> OnlyReflections(List<RectPoint> points)
    {
        List<RectPoint> reflections = new List<RectPoint>();
        foreach (RectPoint point in points)
        {
            int x = Helpers.mod(point.X, Board.Instance.grid.Width);
            int y = Helpers.mod(point.Y, Board.Instance.grid.Height);

            if(!points.Contains(new RectPoint(x,y)))
                reflections.Add(new RectPoint(x, y));
        }

        reflections.RemoveAll(x => !Board.Instance.grid.Contains(x));

        return reflections;
    }

    internal void ShowColorAnimation(Color color, float duration = 0.3f)
    {
        CellVisual anim = Helpers.CreateInstance<CellVisual>("ColorAnimation", this.transform, true);
        anim.sprite.color = color;

        anim.gameObject.AddComponent<Temporary>().mLifespan = duration;

        DOVirtual.Float(0f, 1f, duration, delegate (float fValue) {
            anim.sprite.color = Color.Lerp(color, Helpers.AlphaAdjustedColor(color, 0f), fValue);
        });
    }

    public Quadrant GetQuadrant()
    {
        return Board.Instance.GetQuadrant((RectCell)this);
    }

    protected Color startingColorCache = Color.white;
    internal Color GetStartingColor()
    {
        return startingColorCache;
    }

    [HideInInspector]
    public Vector3 finalPos = Vector3.zero;

    public void TweenToPosition()
    {
        this.transform.DOLocalMove(finalPos, Board.BUILD_GRID_TRAVEL_TIME).SetEase(Ease.OutQuad);
    }

    public bool IsAdjacent(RectCell other)
    {
        return Board.Instance.GetAdjacent(other).Contains(this);
    }

    public bool IsThreatened()
    {
        return ThreateningActors().Count > 0;
    }

    public List<Actor> ThreateningActors()
    {
        List<Actor> threateningActors = new List<Actor>();

        foreach (Actor a in Board.Instance.GetEnemyUnits())
        {
            if (!a.dying)
            {
                if (a.EnemyValidMoves().Contains(this))
                    threateningActors.Add(a);
            }
        }

        return threateningActors;
    }


    public void ShowExplosiveCells()
    {
        List<RectCell> cells = GetExplosiveCells();

        foreach (RectCell c in cells)
            Board.Instance.CreateCellVisual(c, "box-select", Color.red);
    }

    public static List<RectCell> visitedExplosiveCells = new List<RectCell>();
    internal List<RectCell> GetExplosiveCells()
    {
        visitedExplosiveCells.Clear();

        VisitExplosiveCell((RectCell)this);

        List<RectCell> explosiveAffectCells = new List<RectCell>();
        foreach(RectCell c in visitedExplosiveCells)
        {
            foreach(RectCell attackCell in Board.Instance.GetAdjacentMainAndDiagonals(c, true))
            {
                if (!explosiveAffectCells.Contains(attackCell))
                    explosiveAffectCells.Add(attackCell);
            }
        }

        return explosiveAffectCells;
    }

    void VisitExplosiveCell(RectCell cell)
    {
        if (visitedExplosiveCells.Contains(cell))
            return;

        visitedExplosiveCells.Add(cell);

        foreach(RectCell c in Board.Instance.GetAdjacentMainAndDiagonals(cell))
        {
            if (c.GetActorType() == ActorType.Explosive)
                VisitExplosiveCell(c);
        }
    }

    internal void DetonateExplosive(Actor attacker)
    {
        List<RectCell> cells = GetExplosiveCells();

        foreach(RectCell cell in cells)
        {
            cell.Attack(attacker);
        }
    }

    public int NumAdjacentEnemies()
    {
        return Board.Instance.GetAdjacentMainAndDiagonals((RectCell)this).FindAll(x => x.HasEnemy()).Count;
    }

    public int ClosestPlayerUnit()
    {
        int distance = 99;

        foreach(Actor a in Board.Instance.GetPlayerUnits())
        {
            if (a.cell.point.DistanceFrom(this.point) < distance)
                distance = a.cell.point.DistanceFrom(this.point);
        }

        return distance;
    }

    public int NumEmptyAdjacent()
    {
        int count = 0;    
        foreach(RectCell c in Board.Instance.GetAdjacent((RectCell)this))
        {
            if (c.IsPlayerWalkable())
                count++;
        }
        return count;
    }

    public int ViabilityScore()
    {
        return 10 * NumEmptyAdjacent() - ClosestPlayerUnit();
    }

    public List<RectCell> GetAdjacent()
    {
        return Board.Instance.GetAdjacent((RectCell)this);
    }
}

class RectCellComparer : IComparer<RectCell>
{
    public int Compare(RectCell x, RectCell y)
    {
        return y.ViabilityScore() - x.ViabilityScore();
    }
}
