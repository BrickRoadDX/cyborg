﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public enum ResourceType
{
    None = 0,
    Money = 1,
    Turns = 2,
    Escaped = 3,
}

public class ResourceManager : Singleton<ResourceManager>
{
    public Dictionary<ResourceType, int> resourceValues = new Dictionary<ResourceType, int>();
    public Dictionary<ResourceType, TextMeshObject> resourceTextMeshes = new Dictionary<ResourceType, TextMeshObject>();

    public ObjectDisplay resourceTextDisplay;

    public void Init()
    {
        List<ResourceType> resources = Helpers.GetAllEnumTypes<ResourceType>(true);
        if(resourceValues.Count < 1)
        {
            resourceValues.Clear();
            foreach (ResourceType type in resources)
            {
                resourceValues.Add(type, 0);
                TextMeshObject tmo = Helpers.CreateInstance<TextMeshObject>("TextMeshObject", this.transform, true);

                tmo.textMesh.alignment = TextAlignmentOptions.Right;
                resourceTextMeshes.Add(type, tmo);
                resourceTextDisplay.AddObject(tmo);
            }
        }

        foreach(ResourceType type in resources)
        {
            SetValue(type, 0);
        }
    }

    private void UpdateDisplay()
    {
        foreach(ResourceType type in Helpers.GetAllEnumTypes<ResourceType>(true))
        {
            resourceTextMeshes[type].textMesh.text = GetValue(type).ToString();
        }
    }

    public int GetValue(ResourceType type)
    {
        return resourceValues[type];
    }

    public void Gain(ResourceType type, int amount)
    {
        AdjustValue(type, amount);
    }

    public void AdjustValue(ResourceType type, int amount)
    {
        SetValue(type, GetValue(type) + amount);
    }

    public void SetValue(ResourceType type, int value)
    {
        resourceValues[type] = value;
        resourceTextMeshes[type].textMesh.text = string.Format("{0}: {1}", type.ToString(), value.ToString());
    }
}
