﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using DG.Tweening;
using Gamelogic.Grids;
using UnityEngine.SceneManagement;

public enum Direction
{
    None = 0,
    Down = 1,
    Right = 2,
    Up = 3,
    Left = 4,
    DownRight = 5,
    DownLeft = 6,
    UpLeft = 7,
    UpRight = 8,
}

public enum GameSpeed
{
    None = 0,
    Slow = 1,
    Fast = 2,
}

public static class Helpers
{
	private static StringBuilder s_stringBuilder = new StringBuilder(20);
	public static System.Random rng = new System.Random();

	public const int WHILE_LOOP_INSANITY = 10000;
	public static int whileLoopCounter = 0;

    public static bool loading = false;

    public static bool GameActionsBlocked()
    {
        return PopupManager.Instance.BlockGameActions() || CardManager.Instance.CardDisplayUp();
    }

    //pressedUIItem, hitUIitem  (pressed is when mouse down, hit is when mouse up)
    public static tk2dUIItem HeldUiItem() //when clicked
    {
        return tk2dUIManager.Instance.pressedUIItem;
    }

    public static tk2dUIItem HoveredUiItem() //when not clicked
    {
        return tk2dUIManager.Instance.hitUIItem;
    }

    public static bool IsHoveredOrHeld(tk2dUIItem uiItem)
    {
        if (HoveredUiItem() == uiItem)
            return true;

        if (HeldUiItem() == uiItem)
            return true;

        return false;
    }

    public static RectPoint GetRotatedRectPoint(RectPoint point, Direction rotation)
    {
        switch (rotation)
        {
            case Direction.Right:
                return point.Rotate90();
            case Direction.Up:
                return point.Rotate180();
            case Direction.Left:
                return point.Rotate270();
        }
        return point;
    }

    static List<GameSpeed> gameSpeeds = Helpers.GetAllEnumTypes<GameSpeed>();
    static GameSpeed currentSpeed = GameSpeed.None;
    public static void RotateGameSpeed()
    {
        currentSpeed = gameSpeeds.GetNextElement(currentSpeed);

        switch(currentSpeed)
        {
            case GameSpeed.None:
                Time.timeScale = 1f;            
                break;
            case GameSpeed.Slow:
                Time.timeScale = 0.1f;
                break;
            case GameSpeed.Fast:
                Time.timeScale = 3f;
                break;
        }
        Debug.Log(string.Format("Game Speed {0}", currentSpeed));
    }

    public static void AdjustSortingOrders(GameObject go, int amount = 20)
    {
        List<Renderer> renderers = new List<Renderer>();
        foreach (Renderer r in go.GetComponentsInChildren<Renderer>())
            renderers.Add(r);

        foreach (Renderer r in renderers)
            r.sortingOrder = r.sortingOrder + amount;
    }

    internal static void MakeTemporary(GameObject gameObject, float duration)
    {
        gameObject.AddComponent<Temporary>().mLifespan = duration;
    }

    public static Vector3 GetUICameraCenterPosition()
    {
        return GetCameraCenterPosition(Game.Instance.tk2dCamera);
    }

    public static Vector3 GetCameraCenterPosition(Camera cam)
    {
        return cam.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, 10f));
    }

    public static Cooldown MakeCooldown(float duration, Transform parent, bool startImmediately = false)
    {
        Cooldown newCooldown = Helpers.MakeGameObjectWithComponent<Cooldown>(parent);
        newCooldown.StartCooldown(duration);
        if(!startImmediately)
            newCooldown.Pause();
        return newCooldown;
    }

    public static SpriteEffect SpawnSpriteEffect(string animationName, Transform parent, bool permanent = false, int sortingOrder = 28, float scaleMultiplier = 1f, Vector3 offset = default(Vector3))
    {
        SpriteEffect effect = Helpers.CreateInstance<SpriteEffect>("SpriteEffect", parent, true);
        effect.transform.parent = parent;
        effect.transform.localPosition = Vector3.zero;

        effect.transform.localScale = Vector3.one * 5f * scaleMultiplier;
        effect.transform.localPosition += offset;

        foreach (Renderer r in effect.GetComponentsInChildren<Renderer>())
            r.sortingOrder = sortingOrder;

        effect.animator.Play(animationName);

        if (!permanent)
            effect.temporary.mLifespan = (1f / effect.animator.CurrentClip.fps) * effect.animator.CurrentClip.frames.Length;


        return effect;
    }

    public static List<float> GetElementPositions(int numElements, float elementSpacing)
    {
        List<float> elementPositions = new List<float>();
        if (numElements % 2 != 0)
        {
            elementPositions.Add(0f);
            int cardsOutFromMiddle = Mathf.FloorToInt((float)numElements / (float)2);
            for (int i = 0; i < cardsOutFromMiddle; i++)
            {
                float position = (i + 1) * elementSpacing;
                elementPositions.Insert(0, -position);
                elementPositions.Add(+position);
            }
        }
        else
        {
            int elementsOutFromMiddle = Mathf.FloorToInt((float)numElements / (float)2);
            for (int i = 0; i < elementsOutFromMiddle; i++)
            {
                float position = (i + 1) * elementSpacing;
                position -= elementSpacing / 2f;
                elementPositions.Insert(0, -position);
                elementPositions.Add(+position);
            }
        }
        return elementPositions;
    }

    public static RadialProgressBar MakeRadialProgressBar(float fullValue, Transform parent, Vector3 localOffset)
    {
        RadialProgressBar rpb = CreateInstance<RadialProgressBar>("RadialProgressBar", parent);
        rpb.Init(fullValue);
        rpb.transform.parent = parent;
        rpb.transform.localPosition = localOffset;
        return rpb;
    }

    public static ReminderText currentReminderText;
    public static ReminderText ShowReminderText(string text, float scale = 1f, Vector3 origin = default(Vector3), Color textColor = default(Color), bool uiText = true, bool centerText = true)
    {
        if (currentReminderText != null && centerText)
            currentReminderText.gameObject.DestroySelf();

        ReminderText reminderText = Helpers.CreateInstance<ReminderText>("ReminderText");
        reminderText.transform.localScale = Vector3.one * scale;

        reminderText.mover.enabled = false;
        reminderText.fader.enabled = false;

        if (textColor == default(Color))
            textColor = reminderText.text.color;

        reminderText.text.color = textColor;

        if (origin != default(Vector3))
            reminderText.transform.position = origin;
        else
            reminderText.transform.position = Game.Instance.tk2dCamera.transform.position;

        reminderText.fader.endColor = AlphaAdjustedColor(textColor, 0f);

        reminderText.mover.enabled = true;
        reminderText.fader.enabled = true;

        reminderText.text.text = text;

        if (!uiText)
            reminderText.text.gameObject.layer = 1;

        if (centerText)
            currentReminderText = reminderText;

        Debug.Log("Reminder Text: " + text);

        return reminderText;
    }



    public static Vector3 TranslateFromMainWorldToTk2dWorld(Vector3 mainWorldPos)
    {
        Vector3 point = Game.Instance.mainCamera.WorldToScreenPoint(mainWorldPos);
        point = Game.Instance.tk2dCamera.ScreenToWorldPoint(point);
        return point;
    }

    public static Vector3 TranslateFromTk2dWorldtoMainWorld(Vector3 uiPos)
    {
        Vector3 point = Game.Instance.tk2dCamera.WorldToScreenPoint(uiPos);
        point = Game.Instance.mainCamera.ScreenToWorldPoint(point);
        return point;
    }

    public static SpriteRendererGo SpawnSpriteRenderer(string spriteName, Transform parent, Color color = default(Color), int sortingOrder = 0, Vector3 localPosition = default(Vector3))
    {
        SpriteRendererGo effect = CreateInstance<SpriteRendererGo>("SpriteRendererGo", parent, true);
        effect.transform.localPosition = localPosition;

        effect.spriteRenderer.sprite = Helpers.GetSprite(spriteName);
        effect.spriteRenderer.color = color;
        effect.spriteRenderer.sortingOrder = sortingOrder;

        if (color != default(Color))
            effect.spriteRenderer.color = color;
        else
            effect.spriteRenderer.color = Color.white;

        return effect;
    }

    public static TemporarySpriteRenderer SpawnTempSprite(string spriteName, Transform parent, Vector3 position, float duration = 1f, float scaleMultiplier = 1f, bool permanent = false, int sortingOrder = 28)
    {
        TemporarySpriteRenderer effect = Helpers.CreateInstance<TemporarySpriteRenderer>("TemporarySpriteRenderer", parent, true);
        effect.transform.parent = parent;
        effect.transform.position = Vector3.zero;

        effect.transform.localScale = Vector3.one * scaleMultiplier;
        effect.transform.position = position;

        effect.spriteRenderer.sprite = Helpers.GetSprite(spriteName);

        foreach (Renderer r in effect.GetComponentsInChildren<Renderer>())
            r.sortingOrder = sortingOrder;

        if (!permanent)
            effect.temporary.mLifespan = duration;

        return effect;
    }

    public static void LoadSoundValues()
    {
        AudioListener.volume = 0.4f;

        if (!SaveManager.HasKey("soundVolume"))
        {
            SaveManager.SetInt("soundVolume", 40);
        }
        float volume = (float)SaveManager.GetInt("soundVolume") / 100f;

        SoundManager.SetSoundVolume(volume);

        if (!SaveManager.HasKey("musicVolume"))
        {
            SaveManager.SetInt("musicVolume", 40);
        }
        float musicVolume = (float)SaveManager.GetInt("musicVolume") / 100f;

        MusicManager.SetMusicVolume(musicVolume);
    }

    public static int RotateIndex<T>(int index, int increment, List<T> list)
    {
        index += increment;
        index = (list.Count + (index % list.Count)) % list.Count;

        return index;
    }

    public static Vector3 GetVectorDirFromDirection(Direction dir)
    {
        switch (dir)
        {
            case Direction.Up:
                return Vector3.up;
            case Direction.Down:
                return Vector3.down;
            case Direction.Right:
                return Vector3.right;
            case Direction.Left:
                return Vector3.left;
        }

        return Vector3.zero;
    }

    public static Direction GetDirectionFromKeycode(KeyCode keycode)
    {
        switch(keycode)
        {
            case KeyCode.UpArrow:
                return Direction.Up;
            case KeyCode.DownArrow:
                return Direction.Down;
            case KeyCode.RightArrow:
                return Direction.Right;
            case KeyCode.LeftArrow:
                return Direction.Left;
        }
        return Direction.None;
    }

    public static KeyCode GetKeycodeFromDirection(Direction dir)
    {
        switch(dir)
        {
            case Direction.Up:
                return KeyCode.UpArrow;
            case Direction.Down:
                return KeyCode.DownArrow;
            case Direction.Right:
                return KeyCode.RightArrow;
            case Direction.Left:
                return KeyCode.LeftArrow;
        }

        return KeyCode.None;
    }


    public static string AddIcons(string input)
    {
        input = input.Replace("{R}", "<sprite=\"icons\" name=refresh>");
        input = input.Replace("{C}", "<sprite=\"icons\" name=star>");
        input = input.Replace("{S}", "<sprite=\"icons\" name=shield>");
        input = input.Replace("{H}", "<sprite=\"icons\" name=hunt>");
        input = input.Replace("{P}", "<sprite=\"icons\" name=person>");
        input = input.Replace("{A}", "<sprite=\"icons\" name=animal>");
        input = input.Replace("{V}", "<sprite=\"icons\" name=vine>");

        input = input.Replace("{Male}", "<sprite=\"icons\" name=male>");
        input = input.Replace("{Female}", "<sprite=\"icons\" name=female>");

        return input;
    }

    internal static List<Direction> MainDirections()
    {
        return new List<Direction>() { Direction.Up, Direction.Right, Direction.Down, Direction.Left };
    }

    public static bool IsTransformInFrontOfCamera(Camera c, Transform t)
    {
        Vector3 viewportPoint = c.WorldToViewportPoint(t.position);

        return viewportPoint.x.Between(0f, 1f) && viewportPoint.y.Between(0f, 1f) && viewportPoint.z > 0f;
    }

    public static bool TransformInFrontOfCamera(this Camera c, Transform t)
    {
        Vector3 viewportPoint = c.WorldToViewportPoint(t.position);

        return viewportPoint.x.Between(0f, 1f) && viewportPoint.y.Between(0f, 1f);
    }

    public static bool PositionInFrontOfCamera(this Camera c, Vector3 v3)
    {
        Vector3 viewportPoint = c.WorldToViewportPoint(v3);

        return viewportPoint.x.Between(0f, 1f) && viewportPoint.y.Between(0f, 1f);
    }

    public static float GetAngleFromDirection(Direction dir)
    {
        switch (dir)
        {
            case Direction.Up:
                return 90f;
            case Direction.Right:
                return 0f;
            case Direction.Down:
                return -90f;
            case Direction.Left:
                return 180f;

            case Direction.DownLeft:
                return -135f;
            case Direction.DownRight:
                return -45f;
            case Direction.UpLeft:
                return 135f;
            case Direction.UpRight:
                return 45f;
        }

        return 180f;
    }

    public static Quaternion GetRotationFromDirection(Direction dir)
    {
        return Quaternion.Euler(new Vector3(0f, 0f, GetAngleFromDirection(dir)));
    }

    public static bool IsMainMenuScene()
    {
        return SceneManager.GetActiveScene().name.Contains("mainmenu");
    }

    public static bool IsGameScene()
    {
        return SceneManager.GetActiveScene().name.Contains("game");
    }

    public static Direction GetDirectionFromRectPoint(RectPoint p)
    {
        if (p == RectPoint.East)
            return Direction.Right;

        if (p == RectPoint.West)
            return Direction.Left;

        if (p == RectPoint.North)
            return Direction.Up;

        if (p == RectPoint.NorthEast)
            return Direction.UpRight;

        if (p == RectPoint.NorthWest)
            return Direction.UpLeft;

        if (p == RectPoint.SouthWest)
            return Direction.DownLeft;

        if (p == RectPoint.SouthEast)
            return Direction.DownRight;

        return Direction.Down;
    }

    public static RectPoint GetRectPointFromDirection(Direction dir)
    {
        switch(dir)
        {
            case Direction.Up:
                return RectPoint.North;
            case Direction.Down:
                return RectPoint.South;
            case Direction.Left:
                return RectPoint.West;
            case Direction.Right:
                return RectPoint.East;
            case Direction.UpLeft:
                return RectPoint.NorthWest;
            case Direction.UpRight:
                return RectPoint.NorthEast;
            case Direction.DownLeft:
                return RectPoint.SouthWest;
            case Direction.DownRight:
                return RectPoint.SouthEast;
        }

        return RectPoint.Zero;
    }

    public static GameObject SpawnEffect(string resourceName, Transform parent, float duration = 1f, int sortingOrder = 20, float scaleMultiplier = 1f, Vector3 offset = default(Vector3))
    { 
        GameObject effect = Create(resourceName);
        effect.transform.parent = parent;
        effect.transform.localPosition = Vector3.zero;
        effect.gameObject.AddComponent<Temporary>().mLifespan = duration;

        effect.transform.localScale = Vector3.one * 100f * scaleMultiplier;
        effect.transform.localPosition += offset;

        foreach (ParticleSystemRenderer ps in effect.GetComponentsInChildren<ParticleSystemRenderer>())
        {
            ps.sortingOrder = sortingOrder;
        }

        return effect;
    }

    public static GameObject SpawnEffect(string resourceName, Vector3 position, float duration = 1f, int sortingOrder = 20, float scaleMultiplier = 1f)
    {
        GameObject effect = Create(resourceName);
        effect.transform.localPosition = Vector3.zero;
        effect.gameObject.AddComponent<Temporary>().mLifespan = duration;

        effect.transform.localScale = Vector3.one * scaleMultiplier;
        effect.transform.position = position;

        foreach (ParticleSystemRenderer ps in effect.GetComponentsInChildren<ParticleSystemRenderer>())
        {
            ps.sortingOrder = sortingOrder;
        }

        return effect;
    }

    public static HealthBar MakeHealthBar(Transform parent, int maxHp, string prefabName = "HealthBarPip", Vector3 localPos = default(Vector3), Color color = default(Color))
    {
        GameObject go = new GameObject("HealthBar");
        HealthBar hb = go.gameObject.AddComponent<HealthBar>();
        Color c = color;

        hb.Init(maxHp, prefabName, c);
        go.transform.parent = parent;
        hb.transform.localPosition = localPos;
        return hb;
    }


    public static Ray vRay;
	public static RaycastHit hit;
	public static Collider RaycastFromMouse(Camera c)
	{
		vRay = c.ScreenPointToRay(Input.mousePosition);
		Physics.Raycast(vRay, out hit);

		return hit.collider;
	}

	public static Color AlphaAdjustedColor(Color c, float newAlpha) // 1 to 0
	{
		return new Color(c.r, c.g, c.b, newAlpha);
	}

    public static float ScaleFromDirection(RectPoint dir)
    {
        if (!RectPoint.MainDirections.Contains(dir))
            return 0.6f;

        return 1f;
    }

	public static string LowercaseFirst(string s)
	{
		return Char.ToLowerInvariant(s[0]) + s.Substring(1);
	}

	public static Sprite GetSprite(string spriteName)
	{
		return Resources.Load<Sprite>(spriteName);
	}

	public static int mod(int x, int m)
	{
		return (x % m + m) % m;
	}

	public static Vector3 GetGroundMousePosition()
	{
		Plane ground = new Plane(Vector3.up, Vector3.zero);
		Ray ray = Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
		float rayDistance;
		if (ground.Raycast(ray, out rayDistance))
		{
			return ray.GetPoint(rayDistance);
		}
		return new Vector3(0, 0, 0);
	}


	public static int AtLeast(int num, int atLeast)
	{
		return Mathf.Max(num, atLeast);
	}

	public static float AtLeast(float num, float atLeast)
	{
		return Mathf.Max(num, atLeast);
	}

	public static int AtMost(int num, int atMost)
	{
		return Mathf.Min(num, atMost);
	}

	public static float AtMost(float num, float atMost)
	{
		return Mathf.Min(num, atMost);
	}



	public static Vector3 CirclePoint(float percent, float radius, Vector3 center)
	{
		var i = percent;
		// get the angle for this step (in radians, not degrees)
		var angle = i * Mathf.PI * 2;
		// the X &amp; Y position for this angle are calculated using Sin &amp; Cos
		var x = Mathf.Sin(angle) * radius;
		var y = Mathf.Cos(angle) * radius;
		var pos = new Vector3(x, y, 0) + center;

		return pos;
	}

    public static Color HexToColor(string hex)
    {
        byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
        byte a = 255;
        if (hex.Length > 6)
            a = byte.Parse(hex.Substring(6, 2), System.Globalization.NumberStyles.HexNumber);

        return new Color32(r, g, b, a);
    }

    public static Color Color255(int r = 255, int g = 255, int b = 255, int a = 255)
	{
		return new Color(r / 255f, g / 255f, b / 255f, a / 255f);
	}

	public static GameObject Create(string resourceName)
	{
		return (GameObject.Instantiate(Resources.Load(resourceName))) as GameObject;
	}

	public static GameObject Create(string resourceName, Transform parent, bool zeroTransform = false)
	{
		GameObject go = Create(resourceName);
		go.transform.parent = parent;

        if (zeroTransform)
            go.transform.localPosition = Vector3.zero;

		return go;
	}

	public static void Shuffle<T>(this IList<T> list)
	{
		int n = list.Count;
		while (n > 1)
		{
			n--;
			int k = rng.Next(n + 1);
			T value = list[k];
			list[k] = list[n];
			list[n] = value;
		}
	}

    public static void ClearAndDestroyList<T>(this IList<T> list) where T : MonoBehaviour
	{
		foreach (T go in list)
		{
			if (go != null && go.gameObject != null)
				go.gameObject.DestroySelf();
		}

		list.Clear();
	}

    public static void ShuffleNoSeed<T>(this IList<T> list)
	{
		if (list == null)
			return;

		int n = list.Count;
		while (n > 1)
		{
			n--;
			int k = UnityEngine.Random.Range(0, n + 1);
			T value = list[k];
			list[k] = list[n];
			list[n] = value;
		}
	}

	/// <summary>
	/// Pass in an amount in pennies, and will return the value in dollars with a currency symbol.
	/// Example: pass in 325, will return $3.25
	/// </summary>
	/// <param name="amount"></param>
	/// <returns></returns>
	/// 
	static string output;
	public static string FormatCurrency(int amount)
	{
		output = string.Format("${0}.{1}", (amount / 100), (amount % 100).ToString("00")).Replace("-", "");
		if (amount < 0)
		{
			output = "-" + output;
		}
		return output;
	}

	public static float SignedAngle(Vector3 a, Vector3 b)
	{
		var angle = Vector3.Angle(a, b); // calculate angle
										 // assume the sign of the cross product's Y component:
		return angle * Mathf.Sign(Vector3.Cross(a, b).y);
	}

	public static float SignedAngle(Vector2 a, Vector2 b)
	{
		float ang = Vector2.Angle(a, b);
		Vector3 cross = Vector3.Cross(a, b);

		if (cross.z > 0)
			ang = 360 - ang;

		return ang;
	}


	public static bool Between(this float f, float min, float max)
	{
		return f >= min && f <= max;
	}

	/// <summary>
	/// Returns a random int in the range [minValue, maxValue) (ie: exclusive on the upper bound)
	/// </summary>
	public static int RandomIntFromRange(int minValue, int maxValue)
	{
		if (maxValue <= minValue)
		{
			return minValue;
		}



		return rng.Next(minValue, maxValue);
	}

	public static int RandomIntFromRangeInclusive(int minValue, int maxValue)
	{
		return RandomIntFromRange(minValue, maxValue + 1);
	}

	/// <summary>
	/// Returns a random float in the range [minValue, maxValue].
	/// </summary>
	/// 
	///
	static float next;
	public static float RandomFloatFromRangeInclusive(float minValue, float maxValue)
	{
		next = (float)rng.NextDouble();
		return (next * (maxValue - minValue)) + minValue;
	}

	/// <summary>
	/// Same as ChanceRoll, except takes a number from [0f, 1f] instead of [0, 100]
	/// </summary>
	public static bool ChanceRoll(float chance = 0.5f)
	{
		if (chance <= 0f)
		{
			return false;
		}
		else if (chance >= 1f)
		{
			return true;
		}
		else
		{
			if (RandomFloatFromRangeInclusive(0f, 1f) >= chance)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
	}

    public static T MakeGameObjectWithComponent<T>(Transform parent) where T : MonoBehaviour
    {
        GameObject go = new GameObject(typeof(T).FullName);
        go.transform.parent = parent;
        
        return go.AddComponent<T>();
    }

    public static T CreateInstance<T>(string prefabName) where T : MonoBehaviour
	{
		// get the prefab
		GameObject prefab = Resources.Load(prefabName) as GameObject;

		// create a new instance
		GameObject instance = MonoBehaviour.Instantiate(prefab) as GameObject;

		// get the component
		T component = instance.GetComponent<T>();

		return component;
	}

	public static T CreateInstance<T>(string prefabName, Transform parent, bool zeroLocalPosition = false) where T : MonoBehaviour
	{
		// get the prefab
		GameObject prefab = Resources.Load(prefabName) as GameObject;

		// create a new instance
		GameObject instance = MonoBehaviour.Instantiate(prefab) as GameObject;
		instance.transform.SetParent(parent);

		// get the component
		T component = instance.GetComponent<T>();

		if (zeroLocalPosition)
			instance.transform.localPosition = Vector3.zero;

		return component;
	}

	public static T CreateInstance<T>(GameObject prefab) where T : MonoBehaviour
	{
		// create a new instance
		GameObject instance = MonoBehaviour.Instantiate(prefab) as GameObject;

		// get the component
		T component = instance.GetComponent<T>();

		return component;
	}

	public static T CreateInstance<T>(GameObject prefab, Transform parent) where T : MonoBehaviour
	{
		// create a new instance
		GameObject instance = MonoBehaviour.Instantiate(prefab) as GameObject;
		instance.transform.SetParent(parent);

		// get the component
		T component = instance.GetComponent<T>();

		return component;
	}

	public static void StartWhileLoopCounter()
	{
		whileLoopCounter = 0;
	}

	public static bool WhileLoopTick(bool errorOnInsane = true)
	{
		if (Debug.isDebugBuild)
			errorOnInsane = false;

		whileLoopCounter++;
		if (whileLoopCounter >= WHILE_LOOP_INSANITY)
		{
			if (errorOnInsane)
			{
				Debug.LogError("Insanity in while loop");
			}
			else
			{
				Debug.LogWarning("Insanity in while loop");
			}
			return false;
		}
		return true;
	}

	public static void SetTimeScale(float t)
	{
		Time.timeScale = t;
	}

	//	public static void ShowPopup(string text, bool pause = false, Action callback = null)
	//	{
	//		Action modalCallback = null;
	//		if(pause)
	//		{
	//			modalCallback = TimeScaleOne;
	//		}
	//
	//		PopupDataStoryInfo data = new PopupDataStoryInfo("title", L.Get(text), callback, modalCallback);
	//
	//		//PopupStoryInfo popup = PopupManager.Instance.ShowPopupOnStack<PopupStoryInfo>(data);
	//		PopupManager.Instance.Enqueue(data);
	//
	//		//popup.transform.SetPositionX(700f); //HACK
	//
	//		//return popup.transform;
	//	}

	//	public static void ShowPopupTwoButtons(string text, string buttonOneText, string buttonTwoText, bool pause, Action callback = null, Action callbackTwo = null)
	//	{
	//		Action modalCallback = null;
	//		if (pause)
	//		{
	//			TimeScaleZero();
	//			modalCallback = TimeScaleOne;
	//		}
	//
	//		PopupDataStoryInfoTwoButtons data = new PopupDataStoryInfoTwoButtons(text, buttonOneText, buttonTwoText, callback, callbackTwo, modalCallback);
	//
	//		//PopupStoryInfoTwoButtons popup = PopupManager.Instance.ShowPopupOnStack<PopupStoryInfoTwoButtons>(data);
	//		PopupManager.Instance.Enqueue(data);
	//
	//		//popup.transform.SetPositionX(700f); //HACK
	//	}

	public static void ShakeTransform(Transform t, float force = 2f, float duration = 0.3f)
	{
		t.DOGoto(10f);
		t.DOShakePosition(duration, force).SetUpdate(true);
	}

	public static void TimeScaleOne()
	{
		Time.timeScale = 1f;
	}

	public static void TimeScaleZero()
	{
		Time.timeScale = 0f;
	}

	public static void ShuffleFisherYates<T>(List<T> list)
	{
		int n = list.Count;
		for (int i = 0; i < n; i++)
		{
			int r = i + RandomIntFromRange(0, n - i);
			T t = list[r];
			list[r] = list[i];
			list[i] = t;
		}
	}

	// NOTE: this code might seem more complicated than necessary, but using a StringBuilder is the most efficient way to handle this,
	// as it allocates very little memory compared to doing normal string concatenation and string.format calls, which are expensive and allocate a lot of memory
	public static string FormatTimeHoursMinutes(int timeInMinutes, bool regularTime = true)
	{
		// clear the StringBuilder
		s_stringBuilder.Length = 0;

		int hours = ((timeInMinutes % 1440) / 60); // keep in the range of [0,23]
		int minutes = timeInMinutes % 60;

		string postFix = "";

		if (hours == 12) // special case, if hours are exactly 12, is 12 PM
		{
			s_stringBuilder.Append("12");
			postFix = regularTime ? "PM" : "AM";
		}
		else if (hours == 0) // special case, if hours are exactly 0, is 12 AM
		{
			s_stringBuilder.Append("12");
			postFix = regularTime ? "AM" : "PM";
		}
		else if (hours > 11) // anything over 12 and we need to subtract 12 from the hours, and mark as PM
		{
			s_stringBuilder.Append((hours - 12).ToString());
			postFix = regularTime ? "PM" : "AM";
		}
		else // all other cases are AM
		{
			s_stringBuilder.Append(hours.ToString());
			postFix = regularTime ? "AM" : "PM";
		}

		s_stringBuilder.Append(":");

		if (minutes < 10)
		{
			s_stringBuilder.Append("0");
		}

		s_stringBuilder.Append(minutes.ToString());

		s_stringBuilder.Append(" ");
		s_stringBuilder.Append(postFix);

		return s_stringBuilder.ToString();
	}

    internal static string FormatTimeMinutesSeconds(int time)
    {
        return string.Format("{0}:{1}", Mathf.FloorToInt(time / 60), (time % 60).ToString("00"));
    }

    internal static bool IsOdd(int count)
    {
        return (count % 2) != 0;
    }

    public static List<T> GetAllEnumTypes<T>(bool removeNone = false) where T : System.Enum
    {
        List<T> types = new List<T>();

        var values = Enum.GetValues(typeof(T));
        foreach (T type in values)
            types.Add(type);

        if (removeNone)
            types.Remove((T)values.GetValue(0));

        return types;
    }
}