﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public enum RemoveAnim
{
    None = 0,
    Destroy = 1,
    UpAnimation = 2,
    CenterAnimation = 3,
}

public class ObjectDisplay : MonoBehaviour
{
    public Transform objectContainer;

    [HideInInspector]
    public List<MonoBehaviour> displayObjects = new List<MonoBehaviour>();

    [HideInInspector]
    public List<Vector3> storePositions = new List<Vector3>();

    public int rows = 10;
    public int cols = 3;
    public float xOffset = 5f;
    public float yOffset = 6f;

    public float timeToUpdatePositions = 0.3f;

    public int Count { get { return NumItems(); } }

    private void Start()
    {
        float posX = 0f;
        float posY = 0f;

        for (int i = 0; i < rows; i++)
        {
            posX = 0f;
            for (int j = 0; j < cols; j++)
            {
                storePositions.Add(new Vector3(posX, posY, 0f));
                posX += xOffset;
            }
            posY -= yOffset;
        }
    }

    public void UpdatePositions()
    {
        for (int i = 0; i < displayObjects.Count; i++)
        {
            displayObjects[i].transform.parent = objectContainer;
            displayObjects[i].transform.DOLocalMove(storePositions[i], timeToUpdatePositions);

            if (displayObjects[i] is SortingOrderObject)
            {
                ((SortingOrderObject)displayObjects[i]).ResetSortingOrders();
                Helpers.AdjustSortingOrders(displayObjects[i].gameObject, i * 20);
            }
        }
    }

    public void AddObject(MonoBehaviour go)
    {
        displayObjects.Add(go);
        UpdatePositions();
    }

    public List<T> GetCastedList<T>() where T : MonoBehaviour
    {
        List<T> list = new List<T>();
        foreach(T t in displayObjects)
        {
            list.Add(t);
        }

        return list;
    }

    public void RemoveObject(MonoBehaviour mono, RemoveAnim removeProcess = RemoveAnim.None, float animDuration = 1f)
    {
        if (displayObjects.Contains(mono))
            displayObjects.Remove(mono);

        switch(removeProcess)
        {
            case RemoveAnim.None:
                break;
            case RemoveAnim.Destroy:
                mono.gameObject.DestroySelf();
                break;
            case RemoveAnim.UpAnimation:
                mono.transform.DOLocalMoveY(mono.transform.localPosition.y + 5f, animDuration);
                mono.gameObject.DestroySelf(animDuration);
                break;
            case RemoveAnim.CenterAnimation:
                Helpers.AdjustSortingOrders(mono.gameObject, 50 * GetNextCenterAnimSortingIndex());
                mono.transform.DOMove(Helpers.GetUICameraCenterPosition(), animDuration * 0.5f);
                mono.transform.DOScale(mono.transform.localScale.x * 2f, animDuration * 0.5f);
                mono.CallActionDelayed(delegate ()
                {
                    mono.transform.DOShakeRotation(0.5f, new Vector3(0f, 0f, 5f), 45);
                }, animDuration * 0.5f);
                mono.gameObject.DestroySelf(animDuration);
                break;
        }

        UpdatePositions();
    }

    int currIndex = 1;
    public int GetNextCenterAnimSortingIndex()
    {
        currIndex++;
        if (currIndex > 10)
            currIndex = 0;
        return currIndex;
    }

    internal void DestroyAll()
    {
        foreach (MonoBehaviour go in displayObjects)
        {
            if (go != null)
                go.gameObject.DestroySelf();
        }
        displayObjects.Clear();
    }

    public int NumItems()
    {
        return displayObjects.Count;
    }

    internal void RemoveOldestObject(RemoveAnim anim)
    {
        if(displayObjects.Count > 0)
            RemoveObject(displayObjects[0], anim);
    }
}

interface SortingOrderObject
{
    void ResetSortingOrders();
}