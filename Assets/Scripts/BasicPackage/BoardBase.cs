﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Gamelogic.Grids;
using System;
using System.Linq;
using DG.Tweening;

public class BoardBase : Singleton<Board>
{
	public RectGrid<RectCell> grid;

	public IMap3D<RectPoint> map;
	public Camera boardCamera;

	public List<GameObject> cellHighlights = new List<GameObject>();
	public List<RectCell> cellHighlightCells = new List<RectCell>();

    public List<GameObject> cellVisuals = new List<GameObject>();
    public List<RectCell> cellVisualCells = new List<RectCell>();

    public List<Actor> actorsOnBoard = new List<Actor>();

    public override void Awake()
	{
		base.Awake();
		BoardBuildGrid();
	}

    public void ClearBoard()
    {
        foreach (Actor a in actorsOnBoard)
        {
            if(a != null)
                a.gameObject.DestroySelf();
        }

        foreach (RectCell r in AllCellsDontModify())
            r.Init();

        actorsOnBoard.Clear();

        RemoveCellHighlights();
    }

    public void SpawnAppropriateNumber(ActorType type)
    {
        for (int i = 0; i < GenerationValues.NumToSpawn(type); i++)
        {
            List<RectCell> candiates = Board.Instance.GetEmptyNoTerrainCells();

            if(candiates.Count > 0)
                Board.Instance.GetRandomCellFromList(candiates).SpawnActor(type);
        }
    }

    internal bool CellIsAvailalable(RectPoint rectPoint)
    {
        return grid.Contains(rectPoint) && grid[rectPoint].gameObject.activeSelf;
    }

    public void ResetHover()
    {
        if (oldCell != null)
        {
            HoverOut();
            oldCell.HoverOut();
        }

        oldCell = null;
    }

    RectCell oldCell;
	float mTimeOnCell = 0f;
	Vector3 worldPosition;
	RectPoint point;
	RectCell cell;
	float scrollWheelDelay = 0f;
    bool hoveredOnNewCell = false;
    bool hoveredOnNewCellTwo = false;
    void Update()
	{
		//FixMouse();

		Rect screenRect = new Rect(0, 0, Screen.width, Screen.height);
		if (!screenRect.Contains(Input.mousePosition))
			return;

		worldPosition = GridBuilderUtils.ScreenToWorld(this.gameObject, Input.mousePosition);
		point = map[worldPosition];
		cell = !grid.Contains(point) ? null : grid[point];

        if (cell == null && oldCell != null)
        {
            HoverOut();
            oldCell.HoverOut();
        }

        if (Input.GetMouseButtonUp(1) && PopupManager.Instance.DismissablePopup())
            PopupManager.Instance.DismissPopup();

        if (Input.GetMouseButtonUp(1))
        {
            RightClick(cell);
        }

        if (Input.GetMouseButtonUp(0))
		{
            if (cell != null && !Helpers.GameActionsBlocked() && Helpers.HeldUiItem() == null)
				SelectCell(cell);
		}

        if (scrollWheelDelay < 0.1f)
		{
			scrollWheelDelay += Time.deltaTime;
		}
		else
		{
			if (Input.GetAxis("Mouse ScrollWheel") != 0 || Input.GetMouseButtonUp(2) || Input.GetKey(KeyCode.Z) || Input.GetKey(KeyCode.X))
			{
                if(Input.GetAxis("Mouse ScrollWheel") == 0)
                    scrollWheelDelay = 0f;

				bool right = Input.GetAxis("Mouse ScrollWheel") > 0;

                if (Input.GetKey(KeyCode.Z))
                    right = true;

                if (Input.GetKey(KeyCode.X))
                    right = false;

                //do some kind of scroll wheel stuff
                switch (Game.Instance.inputState)
                {
                    case InputState.Playing:
                        
                        break;
                }
			}
		}
	}

    public void CacheEnemyMoves()
    {
        foreach (Actor a in Board.Instance.GetEnemyUnits())
        {
            if (!a.dying)
                a.CacheEnemyMoves();
        }
    }

    public bool cacheAllEnemyMoves = false;
    int numCaches = 0;
    private void LateUpdate()
    {
        if(cacheAllEnemyMoves)
        {
            CacheEnemyMoves();

            cacheAllEnemyMoves = false;

            numCaches++;
            //Debug.Log("Cached enemy moves, {0}", numCaches);
        }

        if (cell != null && !Helpers.GameActionsBlocked())
        {
            bool newCell = oldCell != cell;

            if (newCell)
            {
                if (oldCell != null)
                {
                    oldCell.HoverOut();
                    HoverOut();
                }

                hoveredOnNewCell = false;
                oldCell = cell;
                mTimeOnCell = 0f;
            }
            else
            {
                mTimeOnCell += Time.deltaTime;
            }

            if (cell != null)
            {
                if (mTimeOnCell > 0f && !hoveredOnNewCell)
                {
                    hoveredOnNewCell = true;

                    HoverCell(cell);
                    cell.HoverOver();
                }

                if (mTimeOnCell > 0.3f)
                {

                }
            }
        }
    }

    public List<Actor> GetPlayerUnits()
    {
        return actorsOnBoard.FindAll(x => x.IsPlayerUnit());
    }

    public List<Actor> GetEnemyUnits()
    {
        return actorsOnBoard.FindAll(x => x.IsEnemy());
    }

    internal int NumPlayerUnits()
    {
        return GetPlayerUnits().Count;
    }

    internal Actor SpawnOnClosestEmptySpace(RectCell cell, ActorType capturedUnitType)
    {
        for(int i = 0; i < 8; i++)
        {
            foreach(RectPoint p in PointsAtDistance(cell.point, i))
            {
                if (p.HasCell() && p.ToCell().IsPlayerWalkable() && !p.ToCell().HasPlayerUnit())
                {
                    Actor a = p.ToCell().SpawnActor(capturedUnitType);
                    return a;
                }
            }
        }
        return null;
    }

    internal RectCell GetCellIfAvailable(RectPoint rectPoint)
    {
        if (CellIsAvailalable(rectPoint))
            return grid[rectPoint];

        return null;
    }

    void HoverCell(RectCell cell)
    {

    }

    void HoverOut()
    {

    }

    public bool CanUndo()
    {
        if (Helpers.GameActionsBlocked())
            return false;

        return true;
    }

    void RightClick(RectCell cell)
    {
        if (!CanUndo())
            return;
    }


    internal RectPoint GetCenterPoint()
    {
        return new RectPoint((numCols / 2), (numRows / 2));
    }

    internal RectCell GetCenterCell()
    {
        return grid[GetCenterPoint()];
    }

	public List<RectCell> GetEmptyCells()
	{
        List<RectCell> cells = new List<RectCell>(AllCells());

		cells.RemoveAll(x => x.isOccupied);

		return cells;
	}

    public List<RectCell> GetEmptyNoTerrainCells()
    {
        List<RectCell> cells = GetEmptyCells();
        cells.RemoveAll(x => x.terrainType != TerrainType.None);

        return cells;
    }

    public RectCell GetRandomEmptyAdjacent(RectCell cell)
    {
        List<RectCell> list = GetAdjacent(cell);
        list.RemoveAll(x => x.isOccupied);
        list.Shuffle();

        if(list.Count > 0)
            return list[0];

        return null;
    }

	public List<RectCell> GetAdjacent(RectCell cell)
	{
		List<RectCell> adjSpaces = new List<RectCell>();
		foreach (RectPoint dir in RectPoint.MainDirections)
		{
			RectPoint adj = cell.point + dir;
			if (grid.Contains(adj))
				adjSpaces.Add(grid[adj]);
		}
        adjSpaces.RemoveAll(x => !CellIsAvailalable(x.point));

		return adjSpaces;
	}

	public List<RectCell> GetAdjacentMainAndDiagonals(RectCell cell, bool includeCell = false)
	{
		List<RectCell> adjSpaces = new List<RectCell>();
		foreach (RectPoint dir in RectPoint.MainAndDiagonalDirections)
		{
			RectPoint adj = cell.point + dir;
			if (grid.Contains(adj))
				adjSpaces.Add(grid[adj]);
		}

		if (includeCell)
			adjSpaces.Add(cell);

		return adjSpaces;
	}

    public List<RectCell> GetAdjacentDiagonals(RectCell cell, bool includeCell = false)
    {
        List<RectCell> adjSpaces = new List<RectCell>();
        List<RectPoint> directions = RectPoint.MainAndDiagonalDirections.ToList();
        directions.RemoveAll(x => RectPoint.MainDirections.ToList().Contains(x));
        foreach (RectPoint dir in directions)
        {
            RectPoint adj = cell.point + dir;
            if (grid.Contains(adj))
                adjSpaces.Add(grid[adj]);
        }

        if (includeCell)
            adjSpaces.Add(cell);

        return adjSpaces;
    }

    public List<RectPoint> ShortestPath(RectPoint one, RectPoint two, bool removeStartEnd = false)
	{
        IEnumerable<RectPoint> pointsArray = Algorithms.AStar(
        grid,
        one,
        two,
        (p, q) => p.DistanceFrom(q),
        c => true,
        (p, q) => 1);

        if (pointsArray == null)
            return new List<RectPoint>();

        List<RectPoint> points = pointsArray.ToList();

        if(removeStartEnd)
        {
            points.Remove(one);
            points.Remove(two);
        }

		return points;
	}

    public List<RectPoint> ShortestEnemyPath(RectPoint one, RectPoint two, bool removeStartEnd = false)
    {
        List<RectCell> visible = AllCells();

        IEnumerable<RectPoint> pointsArray = Algorithms.AStar(
        grid,
        one,
        two,
        (p, q) => p.DistanceFrom(q),
        c =>  c.IsEnemyWalkable() && visible.Contains(c),
        (p, q) => 1);

        if (pointsArray == null)
            return new List<RectPoint>();

        List<RectPoint> points = pointsArray.ToList();

        if (removeStartEnd)
        {
            points.Remove(one);
            points.Remove(two);
        }

        return points;
    }

    public Actor ClosestActor(Actor origin, List<Actor> targets)
    {
        int lowDist = 99;
        Actor result = null;

        foreach(Actor a in targets)
        {
            if(a.cell.point.DistanceFrom(origin.cell.point) < lowDist)
            {
                lowDist = a.cell.point.DistanceFrom(origin.cell.point);
                result = a;
            }
        }
        return result;
    }

    bool IsEnemywalkable(RectCell cell)
    {
        return cell.IsEnemyWalkable();
    }

    public List<RectCell> ShortestPathCells(RectPoint one, RectPoint two, bool removeStartEnd = false)
    {
        return ShortestPath(one, two, removeStartEnd).ToCells();
    }

    public RectPoint RandomPointAtDistance(RectPoint origin, int distance, List<RectCell> cells = null)
	{
		List<RectPoint> points = PointsAtDistance(origin, distance, cells);

		if (points.Count == 0) //return origin to indicate not found
			return origin;

		points.Shuffle();

		return points.First();
	}

	public List<RectPoint> PointsAtDistance(RectPoint origin, int distance, List<RectCell> cells = null)
	{
		List<RectPoint> points = grid.ToList();

		if (cells != null)
		{
			points.Clear();
			foreach (RectCell c in cells)
			{
				points.Add(c.point);
			}
		}

		points.RemoveAll(x => x.DistanceFrom(origin) != distance);

        points.Shuffle();

		return points;
	}

    public List<RectPoint> PointsWithinDistance(RectPoint origin, int distance, List<RectCell> cells = null)
    {
        List<RectPoint> points = new RectGrid<RectCell>(numCols, numRows).ToList();

        points.RemoveAll(x => x.DistanceFrom(origin) > distance);

        return points;
    }

    bool SpaceAvailable(RectPoint p)
	{
		return grid.Contains(p) && !grid[p].isOccupied;
	}

	public void HighlightPoints(List<RectPoint> points)
	{
		List<RectCell> cells = new List<RectCell>();

		foreach (RectPoint p in points)
		{
			if (grid.Contains(p))
				cells.Add(grid[p]);
		}

		HighlightCells(cells);
	}

	public void HighlightCells(List<RectCell> cells)
	{
		RemoveCellHighlights();

		foreach (RectCell c in cells)
		{
			CreateCellHighlight(c);
		}
	}

    public RectCell lastSelectedCell;
	public void SelectCell(RectCell cell)
	{
        InputManager.Instance.SelectCell(cell);

        lastSelectedCell = cell;
    }

    public void CreateCellVisuals(List<RectCell> cells, string visualName, Color color = default(Color))
	{
		foreach(RectCell c in cells)
		{
			CreateCellVisual(c, visualName, color);
		}
	}

	public CellVisual CreateCellVisual(RectCell cell, string visualName, Color color = default(Color), bool doubleVisual = false, float sizeMultiplier = 1f, bool addToList = true)
	{
        if (cellVisualCells.Contains(cell) && !doubleVisual)
            return null;

        cellVisualCells.Add(cell);

        CellVisual cv = Helpers.CreateInstance<CellVisual>("BoxSelect");

        if(addToList)
            cellVisuals.Add(cv.gameObject);

        cv.transform.parent = cell.transform;
        cv.transform.localPosition = Vector3.zero;
        cv.transform.localScale = cv.transform.localScale * sizeMultiplier;

        cv.sprite.sprite = Helpers.GetSprite(visualName);

        if (color != default(Color))
            cv.sprite.color = color;

        return cv;
    }

	public void RemoveCellVisuals()
	{
		foreach (GameObject go in cellVisuals)
		{
            go.DestroySelf();
        }

        cellVisuals.Clear();
        cellVisualCells.Clear();
	}

    public void CreateCellHighlight(List<RectCell> cells, Color color = default(Color))
    {
        foreach(RectCell c in cells)
        {
            CreateCellHighlight(c, color);
        }
    }

    public void CreateCellHighlight(RectPoint point, Color color = default(Color))
    {
        RectCell cell = GetCellIfAvailable(point);
        if (cell != null)
            CreateCellHighlight(cell, color);
    }

	public void CreateCellHighlight(RectCell cell, Color color = default(Color))
	{
		if (cellHighlightCells.Contains(cell))
			return;

        CellVisual cv = Helpers.CreateInstance<CellVisual>("CellHighlight");
        cellHighlights.Add(cv.gameObject);
		cv.transform.parent = cell.transform;
		cv.transform.localPosition = Vector3.zero;
        if (color != default(Color))
            cv.sprite.color = color;
		cellHighlightCells.Add(cell);
	}

    public void RemoveCellHighlights()
    {
        foreach (GameObject cellHighlight in cellHighlights)
        {
            cellHighlight.DestroySelf();
        }

        cellHighlightCells.Clear();
        cellHighlights.Clear();
    }

    int numCols = 8;
    int numRows = 8;
    void BoardBuildGrid()
	{
        grid = new RectGrid<RectCell>(numCols, numRows);

		map = new RectMap(new Vector2(6f, 6f))
			.WithWindow(boardCamera.rect)
				.AlignMiddleCenter(grid)
				.Translate(new Vector2(0f, 0f))
				.To3DXY();

        foreach (RectPoint point in grid)
        {
            CreateCell(point);
        }
    }

    public static float BUILD_GRID_TRAVEL_TIME
    {
        get
        {
            #if UNITY_EDITOR
                return 1f;
            #endif

            return 0.3f;
        }
    }

    public static float BUILD_GRID_INTERVAL
    {
        get
        {
            #if UNITY_EDITOR
                return 0.01f;
            #endif

            return  0.01f;
        }
    }

    public RectCell CreateCell(RectPoint point)
    {
        RectCell cell = Helpers.CreateInstance<RectCell>("RectCell");

        cell.transform.parent = this.transform;

        //float tweenDuration = 1f;
        cell.transform.localScale = Vector3.one;

        cell.SetColor(cell.GetStartingColor());

        cell.Init(grid, point);
        grid[point] = cell;

        return cell;
    }

    private List<RectCell> allCellsCache = new List<RectCell>();
    internal List<RectCell> AllCellsDontModify()
    {
        if (allCellsCache.Count > 0)
            return allCellsCache;

        foreach (RectPoint point in grid)
        {
            if (grid.Contains(point) && grid[point] != null)
                allCellsCache.Add(grid[point]);
        }
        return allCellsCache;
    }

    internal List<RectCell> AllCells()
	{
        List<RectCell> list = new List<RectCell>();
        foreach(RectPoint point in grid)
        {
            if (grid.Contains(point) && grid[point] != null)
                list.Add(grid[point]);
        }
        return list;
	}

	public List<RectCell> GetRowColCross(RectCell cell)
	{
		List<RectCell> cells = new List<RectCell>();
		foreach (RectPoint p in grid)
		{
			if (p.Y == cell.point.Y || p.X == cell.point.X)
				cells.Add(grid[p]);
		}
		cells.Remove(cell);
		return cells;
	}

	internal List<RectCell> GetRow(int rowIndex)
	{
		List<RectCell> cells = new List<RectCell>();
		foreach (RectPoint p in grid)
		{
			if (p.Y == rowIndex)
				cells.Add(grid[p]);
		}
		return cells;
	}

    internal List<RectCell> GetCol(int colIndex)
	{
		List<RectCell> cells = new List<RectCell>();
		foreach (RectPoint p in grid)
		{
			if (p.X == colIndex)
				cells.Add(grid[p]);
		}
		return cells;
	}


	internal List<RectCell> GetBorder()
	{
		List<RectCell> cells = new List<RectCell>();
		cells.AddRange(GetRow(0));
		cells.AddRange(GetRow(grid.Height - 1));

		cells.AddRange(GetCol(0));
		cells.AddRange(GetCol(grid.Width - 1));

		return cells.Distinct().ToList();
	}

	public RectCell GetRandomCellFromList(List<RectCell> cells)
	{
		return cells[Helpers.RandomIntFromRange(0, cells.Count)];
	}

    public RectCell GetCell(int row, int col)
	{
		if (grid.Contains(new RectPoint(row, col)))
			return grid[new RectPoint(row, col)];

		Debug.Log(string.Format("Row {0}, Col {1} does not exist", row, col));
		return grid[new RectPoint(0, 0)];
	}

    public List<RectPoint> GetAdjacentPattern(RectCell origin)
    {
        List<RectPoint> points = new List<RectPoint>();
        points.Add(origin.point);
        foreach (RectPoint dir in RectPoint.MainDirections)
        {
            points.Add(origin.point + dir);
        }

        return points;
    }

    public List<RectPoint> GetTwoMovePattern(RectCell origin)
    {
        List<RectPoint> points = new List<RectPoint>();
        points.Add(origin.point);
        foreach(RectPoint dir in RectPoint.MainDirections)
        {
            points.Add(origin.point + dir);
            points.Add(origin.point + dir + dir);
            points.Add(origin.point + dir + dir.Rotate90());
        }

        return points;
    }

    public List<RectPoint> GetMovesFromMovespeed(RectCell origin, int moveSpeed = 0)
	{
		Dictionary<RectPoint, int> moves = Algorithms.GetPointsInRangeCost<RectCell, RectPoint>(
			grid,
			origin.point,
			c => c.IsPlayerWalkable(),
			(p, q) => 1,
			moveSpeed);

		return moves.Keys.ToList();
	}

    public List<Actor> ActorsOfType(ActorType a)
    {
        return actorsOnBoard.FindAll(x => x.type == a);
    }

    public RectPoint GetReflectionPoint(RectPoint firstPoint)
    {
        int x = Helpers.mod(firstPoint.X, Board.Instance.grid.Width);
        int y = Helpers.mod(firstPoint.Y, Board.Instance.grid.Height);

        RectPoint point = new RectPoint(x, y);

        if (grid.Contains(point))
            return point;

        return firstPoint;
    }


    public RectCell GetMirrorCell(RectCell firstCell)
    {
        RectPoint other = new RectPoint(cell.grid.Width - firstCell.point.X - 1, cell.grid.Height - firstCell.point.Y - 1);
        return GetCellIfAvailable(other);
    }

    public Quadrant GetQuadrant(RectCell cell)
    {
        if (cell.point.X < 4 && cell.point.Y >= 4)
            return Quadrant.One;

        if (cell.point.X >= 4 && cell.point.Y >= 4)
            return Quadrant.Two;

        if (cell.point.X < 4 && cell.point.Y < 4)
            return Quadrant.Three;

        if (cell.point.X >= 4 && cell.point.Y < 4)
            return Quadrant.Four;

        return Quadrant.None;
    }

    public ActorType ActorTypeInDirection(RectCell cell, Direction d)
    {
        RectCell newCell = (cell.point + Helpers.GetRectPointFromDirection(d)).ToCell();
        if (newCell != null)
            return newCell.GetActorType();

        return ActorType.None;
    }

    internal bool FirstObjectIsEnemy(RectCell cell, Direction d)
    {
        RectCell curr = cell;
        RectPoint dir = Helpers.GetRectPointFromDirection(d);

        while(curr != null)
        {
            curr = GetCellIfAvailable(curr.point + dir);
            if(curr != null)
            {
                if (curr.HasEnemy())
                    return true;

                if (curr.isOccupied)
                    return false;
            }
        }

        return false;
    }

    internal bool FirstObjectIsPlayer(RectCell cell, Direction d)
    {
        RectCell curr = cell;
        RectPoint dir = Helpers.GetRectPointFromDirection(d);

        while (curr != null)
        {
            curr = GetCellIfAvailable(curr.point + dir);
            if (curr != null)
            {
                if (curr.HasPlayerUnit())
                    return true;

                if (curr.isOccupied)
                    return false;
            }
        }

        return false;
    }

    public RectPoint FirstRectPointToAttack(RectCell cell, Direction d, bool stayOnBoard = false)
    {
        RectPoint curr = cell.point;
        RectPoint dir = Helpers.GetRectPointFromDirection(d);

        RectPoint lastOnBoard = cell.point;

        while (grid.Contains(curr))
        {
            lastOnBoard = curr;
            curr = curr + dir;
            if (curr.ToCell())
            {
                if (curr.ToCell().isOccupied)
                    return curr;
            }
        }

        if (!curr.HasCell() && stayOnBoard)
            return lastOnBoard;

        return curr;
    }

    public int FirstRectPointDistance(RectCell cell, Direction d)
    {
        RectPoint dest = FirstRectPointToAttack(cell, d, true);
        return cell.point.DistanceFrom(dest);
    }

    Sequence buildGrid;
    public void BuildGridAnimation()
    {
        buildGrid = DOTween.Sequence();

        foreach (RectPoint point in grid)
        {
            float circleDistance = GetBoardWidth() * 2f;
            RectCell cell = grid[point];
            Vector3 pos = Helpers.CirclePoint(Helpers.RandomFloatFromRangeInclusive(0f, 1f), circleDistance, cell.finalPos);
            cell.transform.DOKill();
            cell.finalPos = map[point];
            cell.transform.position = pos;
            buildGrid.AppendCallback(cell.TweenToPosition);
            buildGrid.AppendInterval(BUILD_GRID_INTERVAL);
        }

        buildGrid.Play();
    }

    public float GetBoardWidth()
    {
        return map[new RectPoint(numCols, 0)].x - map[new RectPoint(0, 0)].x;
    }

    public RectCell HoveredCell()
    {
        Vector3 worldPosition = GridBuilderUtils.ScreenToWorld(this.gameObject, Input.mousePosition);
        RectPoint point = map[worldPosition];
        RectCell hovered = !grid.Contains(point) ? null : grid[point];

        return hovered;
    }

    public List<RectPoint> GetCleavePoints(RectPoint origin, RectPoint target, int range = 2)
    {
        List<RectPoint> points = new List<RectPoint>();

        points.Add(target);

        if (origin.X != target.X) //if the attack is to the right or left
        {
            points.Add(target + RectPoint.South);
            points.Add(target + RectPoint.North);

            if(range > 2)
            {
                points.Add(target + + RectPoint.South + RectPoint.South);
                points.Add(target + RectPoint.North + RectPoint.North);
            }
        }

        if (origin.Y != target.Y) //if the attack is above or below the warrior
        {
            points.Add(target + RectPoint.East);
            points.Add(target + RectPoint.West);

            if (range > 2)
            {
                points.Add(target + +RectPoint.East + RectPoint.East);
                points.Add(target + RectPoint.West + RectPoint.West);
            }
        }

        return points;
    }

    public List<RectPoint> GetSpearPoints(RectPoint origin, RectPoint target, int distance = 2)
    {
        List<RectPoint> points = new List<RectPoint>();

        points.Add(target);
        RectPoint dir = target - origin;
        points.Add(origin + dir + dir);

        if (distance > 2)
            points.Add(origin + dir + dir + dir);

        return points;
    }

    public List<RectPoint> GetXPoints(RectPoint origin)
    {
        List<RectPoint> points = new List<RectPoint>();

        points.Add(origin);
        foreach(RectPoint p in RectPoint.MainDirections)
        {
            points.Add(origin + p + p.Rotate90());
        }

        return points;
    }

    public List<RectPoint> GetAdjacentMainAndDiagonalPattern(RectPoint origin, bool inclueCell = false)
    {
        List<RectPoint> points = new List<RectPoint>();

        if(inclueCell)
            points.Add(origin);

        foreach (RectPoint p in RectPoint.MainAndDiagonalDirections)
        {
            points.Add(origin + p);
        }

        return points;
    }
}

public enum Quadrant
{
    None = 0,
    One = 1,
    Two = 2,
    Three = 3,
    Four = 4,
}
