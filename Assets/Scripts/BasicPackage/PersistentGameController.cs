﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersistentGameController : Singleton<PersistentGameController> {

    public static bool saveWaiting = false;

    public override void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    void Update()
    {
        if (SaveManager.saveDelay >= 0f)
        {
            SaveManager.saveDelay -= Time.deltaTime;

            if (SaveManager.saveDelay <= 0f && saveWaiting)
                SaveManager.SaveGame(true);
        }
    }
}
