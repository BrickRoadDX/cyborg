﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoverZoom : MonoBehaviour
{
    public tk2dUIItem uiItem;

    Tweener tweener;

    float currentScale = 1f;
    public float scaleFactor = 1.2f;
    public float scaleDuration = 0.3f;
    public Ease ease = Ease.OutQuad;
    public bool fasterHoverIn = true;

    private void Awake()
    {
        currentScale = this.transform.localScale.x;
        tweener = this.transform.DOScale(currentScale * scaleFactor, scaleDuration).SetEase(ease).SetAutoKill(false).Pause();
    }

    bool isHovered = false;
    private void Update()
    {
        //Debug.Log(Helpers.HoveredUiItem());

        if(!isHovered)
        {
            if (Helpers.IsHoveredOrHeld(uiItem))
            {
                isHovered = true;
                HandleHover();
            }
        }
        else
        {
            if (!Helpers.IsHoveredOrHeld(uiItem))
            {
                isHovered = false;
                HandleHoverOut();
            }
        }
    }

    private void HandleHover()
    {
        if(fasterHoverIn)
            tweener.timeScale = 4f;

        tweener.PlayForward();
    }

    private void HandleHoverOut()
    {
        tweener.timeScale = 1f;
        tweener.PlayBackwards();
    }


}
