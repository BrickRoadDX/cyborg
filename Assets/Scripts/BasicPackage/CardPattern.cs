﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Gamelogic.Grids;
using System;

public class CardPattern
{
    public List<PatternSpace> patternSpaces = new List<PatternSpace>();

    public void AddPatternSpace(PatternSpace toAdd)
    {
        patternSpaces.Add(toAdd);
    }

    public static CardPattern GetCardPatternForCardType(CardType cardType)
    {
        CardPattern pattern = new CardPattern();

        switch (cardType)
        {
            case CardType.Knight:
                pattern.AddPatternSpace(new PatternSpace(new RectPoint(0, 0), EffectType.Start));
                pattern.AddPatternSpace(new PatternSpace(new RectPoint(0, 1), EffectType.Blank));
                pattern.AddPatternSpace(new PatternSpace(new RectPoint(0, 2), EffectType.Blank));
                pattern.AddPatternSpace(new PatternSpace(new RectPoint(1, 2), EffectType.End));
                break;
        }

        pattern.patternSpaces.Sort((PatternSpace x, PatternSpace y) => (int)x.effectType - (int)y.effectType);

        return pattern;
    }

    public static string GetEffectTypeSprite(EffectType type)
    {
        switch (type)
        {
            case EffectType.Start:
                return "box-select";
            case EffectType.End:
                return "question";
        }

        return "clear";
    }

    public PatternSpace GetSpaceWithType(EffectType type)
    {
        foreach (PatternSpace space in patternSpaces)
        {
            if (space.effectType == type)
                return space;
        }
        return null;
    }

    internal bool HasSpaceWithType(EffectType type)
    {
        return GetSpaceWithType(type) != null;
    }

    internal static float GetXOffset(CardType type)
    {
        switch (type)
        {

        }

        return 0f;
    }

    internal static float GetYOffset(CardType type)
    {
        switch (type)
        {

        }

        return 0f;
    }


}

public enum EffectType
{
    None = 0,
    Blank = 1,
    Start = 2,
    End = 3,
}

public class PatternSpace
{
    public RectPoint position;
    public EffectType effectType;
    public RectPoint direction = RectPoint.Zero;

    public PatternSpace(RectPoint position, EffectType eType, RectPoint direction = default(RectPoint))
    {
        this.position = position;
        this.effectType = eType;
        this.direction = direction;
    }
}