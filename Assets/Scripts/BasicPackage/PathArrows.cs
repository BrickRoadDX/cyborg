﻿using Gamelogic.Grids;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PathArrows 
{
    List<SpriteRendererGo> pathArrows = new List<SpriteRendererGo>();
    List<RectCell> path = new List<RectCell>();
    Actor actor;

    Color arrowColor = Color.yellow * 0.5f;

    public RectCell NextStep()
    {
        return path[0];
    }

    internal void Init(ActorBase actorBase)
    {
        actor = (Actor)actorBase;
    }

    public void TryAddToPath(RectCell cell, bool lookForShorterPaths = true)
    {
        RectCell firstAdjacent = null;

        if(lookForShorterPaths)
        {
            if (actor.cell.GetAdjacent().Contains(cell))
                firstAdjacent = actor.cell;

            for(int x = 0; x < path.Count; x++)
            {
                RectCell c = path[x];
                if (firstAdjacent == null)
                {
                    if(c.GetAdjacent().Contains(cell))
                        firstAdjacent = c;
                }
                else
                {
                    path.RemoveAt(x);
                    x--;
                }
            }
        }
        else
        {
            if (path.Count < 1)
            {
                if(actor.cell.GetAdjacent().Contains(cell))
                    firstAdjacent = actor.cell;
            }
            else
            {
                if (path.Last().GetAdjacent().Contains(cell))
                    firstAdjacent = path.Last();
            }
        }

        if (firstAdjacent == null)
            return;

        if (!path.Contains(cell))
        {
            path.Add(cell);
            ShowPathArrows();
        }
    }

    internal void SetPath(List<RectPoint> path)
    {
        this.path = path.ToCells();
        ShowPathArrows();
    }

    RectCell lastPathArrowCell;
    RectPoint lastDir;
    public void ShowPathArrows()
    {
        RemovePathArrows();

        for (int i = 0; i < path.Count; i++)
        {
            lastPathArrowCell = actor.cell;
            if (i > 0)
                lastPathArrowCell = path[i - 1];

            RectPoint dir = path[i].point - lastPathArrowCell.point;

            if (lastDir != dir && pathArrows.Count > 0)
                SetToTurnArrow(pathArrows.Last(), dir, lastDir);

            CreateStraightArrow(path[i], dir);

            lastDir = dir;
        }
    }

    internal void RemoveNextStep()
    {
        path.RemoveAt(0);
        ShowPathArrows();
    }

    internal bool IsEmpty()
    {
        return path.Count < 1;
    }

    SpriteRendererGo CreateArrow()
    {
        SpriteRendererGo srgo = Helpers.SpawnSpriteRenderer("straight-arrow", Board.Instance.transform, arrowColor, 20);
        srgo.transform.localScale = Vector3.one * 0.6f;
        return srgo;
    }

    SpriteRendererGo newSRGO;
    public void CreateStraightArrow(RectCell cell, RectPoint dir)
    {
        newSRGO = CreateArrow();
        RectPoint nextPoint = cell.point + dir;
        newSRGO.transform.position = cell.point.PointPos();

        if (cell == path.Last())
        {
            newSRGO.transform.position = Vector3.Lerp(lastPathArrowCell.point.PointPos(), cell.point.PointPos(), 0.5f);
            newSRGO.spriteRenderer.sprite = Helpers.GetSprite("plain-arrow");
        }
        else
        {
            SpriteRendererGo middleSrgo = CreateArrow();
            middleSrgo.transform.position = Vector3.Lerp(lastPathArrowCell.point.PointPos(), cell.point.PointPos(), 0.5f);
            pathArrows.Add(middleSrgo);
            SetArrowRotation(dir, middleSrgo);
        }

        pathArrows.Add(newSRGO);

        SetArrowRotation(dir, pathArrows.Last());
    }

    void SetArrowRotation(RectPoint dir, SpriteRendererGo go)
    {
        if (dir.X == 1)
            go.transform.localRotation = Quaternion.Euler(0f, 0f, 90f);

        if (dir.X == -1)
            go.transform.localRotation = Quaternion.Euler(0f, 0f, -90f);

        if (dir.Y == 1)
            go.transform.localRotation = Quaternion.Euler(0f, 0f, 180f);

        if (dir.Y == -1)
            go.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
    }

    public void SetToTurnArrow(SpriteRendererGo srgo, RectPoint dir, RectPoint lastDir)
    {
        srgo.spriteRenderer.sprite = Helpers.GetSprite("turn-bottom-right");

        if (lastDir == RectPoint.East && dir == RectPoint.North)
            srgo.transform.localRotation = Quaternion.Euler(0f, 180f, 90f);

        if (lastDir == RectPoint.East && dir == RectPoint.South)
            srgo.transform.localRotation = Quaternion.Euler(0f, 180f, 0f);

        if (lastDir == RectPoint.West && dir == RectPoint.North)
            srgo.transform.localRotation = Quaternion.Euler(0f, 0f, 90f);

        if (lastDir == RectPoint.West && dir == RectPoint.South)
            srgo.transform.localRotation = Quaternion.Euler(180f, 0f, 90f);

        if (lastDir == RectPoint.North && dir == RectPoint.East)
            srgo.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);

        if (lastDir == RectPoint.North && dir == RectPoint.West)
            srgo.transform.localRotation = Quaternion.Euler(0f, 180f, 0f);

        if (lastDir == RectPoint.South && dir == RectPoint.East)
            srgo.transform.localRotation = Quaternion.Euler(0f, 180f, 180f);

        if (lastDir == RectPoint.South && dir == RectPoint.West)
            srgo.transform.localRotation = Quaternion.Euler(0f, 0f, 180f);
    }

    public void RemovePathArrows()
    {
        foreach (SpriteRendererGo pathArrow in pathArrows)
        {
            pathArrow.gameObject.DestroySelf();
        }

        pathArrows.Clear();
    }
}
