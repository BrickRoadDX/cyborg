﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class PingPongScaler : MonoBehaviour
{
    public Ease ease = Ease.Linear;

    public Vector3 scaleAmounts = Vector3.one * 1.1f;
    private Vector3 startScale;
    public float duration = 1f;

    Tweener tween;

    public bool once = false;

    public void SetValues(Vector3 scaleAmounts, float duration, Ease ease = Ease.Linear)
    {
        PauseTween();
        this.scaleAmounts = scaleAmounts;
        this.duration = duration;
        this.ease = ease;
        PlayTween();
    }

    void PauseTween()
    {
        tween.Kill();

        transform.localPosition = startScale;
    }

    void PlayTween()
    {
        startScale = transform.localScale;
        Vector3 finalScale = new Vector3(startScale.x * scaleAmounts.x, startScale.y * scaleAmounts.y, startScale.z * scaleAmounts.z);
        tween = transform.DOScale(finalScale, duration).SetUpdate(false);
        tween.SetEase(ease).SetLoops(-1, LoopType.Yoyo).ChangeStartValue(startScale);

        if (once)
            tween.SetLoops(1);

        tween.Play();
    }

    void OnEnable()
    {
        PlayTween();
    }

    void OnDisable()
    {
        PauseTween();
    }
}
