﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public enum CardPileType
{
    None  = 0,
    Discard = 1,
}

public class CardPile : MonoBehaviour
{
    public tk2dUIItem uiItem;
    public CardPileType cardPileType = CardPileType.Discard;
    public TextMeshPro numCardsText;
    public ObjectDisplay cardDisplay;

    public SpriteRenderer fadeSprite;

    bool displayIsUp = false;

    private void Awake()
    {
        displayIsUp = false;
        CardManager.Instance.cardPiles.Add(this);
        fadeSprite.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        uiItem.OnClick += HandleClicked;
    }

    private void OnDisable()
    {
        uiItem.OnClick -= HandleClicked;
    }

    private void HandleClicked()
    {
        if (displayIsUp)
            return;

        foreach(Card card in GetCardList())
        {
            CardObject co = CardManager.CreateCard(this.transform, card);
            cardDisplay.AddObject(co);
        }

        foreach(CardObject co in cardDisplay.GetCastedList<CardObject>())
        {
            Helpers.AdjustSortingOrders(co.gameObject, 100);
        }

        SetDisplayUp(true);
    }

    void SetDisplayNotUp()
    {
        SetDisplayUp(false);
    }

    void SetDisplayUp(bool displayUp)
    {
        fadeSprite.gameObject.SetActive(displayUp);
        displayIsUp = displayUp;
    }

    void ClearCardDisplay()
    {
        cardDisplay.DestroyAll();
    }

    private void Update()
    {
        if(displayIsUp)
        {
            if(Input.GetMouseButtonUp(0) || Input.GetMouseButtonUp(1))
            {
                ClearCardDisplay();
                this.CallActionDelayed(SetDisplayNotUp);
            }
        }
    }

    public List<Card> GetCardList()
    {
        switch(cardPileType)
        {
            case CardPileType.Discard:
                return CardManager.Instance.discard;
        }

        return new List<Card>();
    }

    public void UpdateCardPile()
    {
        numCardsText.text = GetCardList().Count.ToString();
    }

    internal bool DisplayUp()
    {
        return displayIsUp;
    }
}
