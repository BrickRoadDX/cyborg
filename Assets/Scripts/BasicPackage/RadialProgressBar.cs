﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RadialProgressBar : MonoBehaviour
{
    public GameObject visualGameObject;

    public Image radialProgressFill;

    private float fullValue = 1f;
    private float currentValue = 1f;

    bool hideIfFull = true;

    public void Init(float fullValue = 1f, bool hideIfFull = true)
    {
        this.hideIfFull = hideIfFull;
        this.fullValue = fullValue;
        this.currentValue = fullValue;
        UpdateVisuals();
    }

    public void AdjustBarValue(float amount)
    {
        currentValue += amount;
        UpdateVisuals();
    }

    public void SetBarValue(float newValue)
    {
        currentValue = newValue;
        UpdateVisuals();
    }

    private void UpdateVisuals()
    {
        radialProgressFill.fillAmount = currentValue / fullValue;

        if(hideIfFull)
            visualGameObject.gameObject.SetActive(!IsFull());
    }

    internal bool IsFull()
    {
        return currentValue >= fullValue;
    }

    internal bool IsEmpty()
    {
        return currentValue < 0f;
    }

    internal void Reset()
    {
        currentValue = fullValue;
        UpdateVisuals();
    }
}
