﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;
using System.Collections.Generic;

public class MusicManager : Singleton<MusicManager>
{
	public AudioClip[] theClips;

	public AudioClip[] clips
	{
		get
		{
			return theClips;
		}
	}
	public AudioSource audioSource;
	private static float musicVolume = 0.2f;

    List<AudioClip> upNextClips = new List<AudioClip>();
    public AudioClip GetNextClip()
    {
        if (upNextClips.Count < 1)
        {
            upNextClips = theClips.ToList();
            upNextClips.Shuffle();
        }

        return upNextClips.PopFirst();
    }

    public void PlayClip(string clipName)
    {
        audioSource.clip = Resources.Load(clipName) as AudioClip;
        UpdateCurrentClipText();
        audioSource.Play();
    }

    public void PlayAudioClip(AudioClip clip)
    {
        audioSource.clip = clip;
        UpdateCurrentClipText();
        audioSource.Play();
    }

    void Start()
	{
		UpdateVolume();
	}

	void OnEnable()
	{
		UpdateVolume();
	}

	public static void SetMusicVolume(float value)
	{
		musicVolume = value;

		if (MusicManager.HasPrivateInstance())
		{
			MusicManager.Instance.UpdateVolume();
		}
	}

	void UpdateVolume()
	{
		audioSource.volume = musicVolume;
	}

	void Update()
	{
		if (!audioSource.isPlaying && !isPaused)
		{
			PlayNextClip();
		}
	}

	public void PlayNextClip()
	{
        PlayAudioClip(GetNextClip());
	}

    void UpdateCurrentClipText()
    {
        //if (currentClipText != null)
        //{
            string trackName = audioSource.clip.name.Length > 15 ? audioSource.clip.name.Substring(0, 15) : audioSource.clip.name;

            trackName = audioSource.clip.name;

        //    currentClipText.text = trackName + ".mp3";
        //}
        Debug.Log(string.Format("Music Manager Now Playing {0}", trackName));
    }

	void StopAudioSources()
	{
		audioSource.Pause();
	}

	public string CurrentClipName()
	{
		if (audioSource.clip != null)
		{
			return audioSource.clip.name;
		}
		else
		{
			return "";
		}
	}

    bool isPaused = false;

    void OnApplicationFocus(bool hasFocus)
    {
        isPaused = !hasFocus;
    }

    void OnApplicationPause(bool pauseStatus)
    {
        isPaused = pauseStatus;
    }

}
