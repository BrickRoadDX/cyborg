﻿using Gamelogic.Grids;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy
{
    public Actor actor;
    public PathArrows actorPath = new PathArrows();

    public Enemy(Actor thisActor)
    {
        actor = thisActor;
        actorPath.Init(actor);
    }

    [HideInInspector]
    public List<RectCell> validMoves = new List<RectCell>();
    internal void CacheEnemyMoves()
    {
        List<RectPoint> candidates = new List<RectPoint>();

        switch (actor.type)
        {
            case ActorType.Guard:
                if (actorPath.IsEmpty())
                    MakeNewPath();

                if (actorPath.IsEmpty())
                    return;

                if (!actorPath.NextStep().HasEnemy())
                {
                    actor.Move(actorPath.NextStep().point);
                    actorPath.RemoveNextStep();
                }
                else
                {
                    actorPath.RemovePathArrows();
                }

                if (actorPath.IsEmpty())
                    MakeNewPath();

                break;
        }
    }

    public void MakeNewPath()
    {
        if (Board.Instance.GetPlayerUnits().Count < 1)
            return;

        List<RectPoint> path = BoardBase.Instance.ShortestEnemyPath(actor.cell.point, Board.Instance.ClosestActor(actor, Board.Instance.GetPlayerUnits()).cell.point);
        path.RemoveAt(0);
        actorPath.SetPath(path);
    }

    public void EnteredBoard()
    {
        switch(actor.type)
        {
            case ActorType.Guard:
                MakeNewPath();
                break;
        }
    }

    internal void DoEnemyMove()
    {
        if (Game.Instance.inputState != InputState.EnemyTurn)
            return;

        if (validMoves.Count > 1)
            validMoves.RemoveAll(x => x == actor.previousPosition && !x.HasPlayerUnit());
    }

    internal void DoPreMove(Action donePreMoveCallback)
    {
        donePreMoveCallback();
    }
}
