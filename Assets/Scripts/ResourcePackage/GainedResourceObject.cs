﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GainedResourceObject : MonoBehaviour {

    public tk2dSprite sprite;
    public tk2dSpriteAnimator animator;

    public void SetSprite(string spriteName)
    {
        sprite.SetSprite(spriteName);
    }

    public void SetSpriteAnimation(string animName)
    {
        if (animName.Length < 1)
            return;

        animator.Play(animName);
    }
}
