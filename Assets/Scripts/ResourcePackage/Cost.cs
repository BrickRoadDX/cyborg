﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Cost
{
	public List<int> values = new List<int>();

	public Cost(int hydrogen = 0, int oxygen = 0, int metal = 0, int cards = 0, int colonists = 0)
	{
		values = new List<int> { hydrogen, oxygen, metal, cards, colonists };
	}

	public void Set(ResourceType type, int value)
	{
		values[((int)type) - 1] = value;
	}

	public int Get(ResourceType type)
	{
		return values[((int)type) - 1];
    }

	public void Add(ResourceType type, int amount)
	{
		Set(type, Get(type) + amount);
	}

    internal bool HasCost()
    {
        return values[0] > 0 || values[1] > 0 || values[2] > 0 || values[3] > 0 || values[4] > 0;
    }

    public string CostString()
    {
        if (!HasCost())
            return "";

        string costString = "Cost: ";

        costString += ResourceString();

        return costString;
    }
    
    public string ResourceString()
    {
        string resourceString = "";

        if (values[0] > 0)
            resourceString += "{H} " + values[0];

        if (values[1] > 0)
            resourceString += "{O} " + values[1];

        if (values[2] > 0)
            resourceString += "{M} " + values[2];

        if (values[3] > 0)
            resourceString += "{C} " + values[3];

        if (values[4] > 0)
            resourceString += "{P} " + values[4];

        return resourceString;
    }
}