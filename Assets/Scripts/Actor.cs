﻿using DG.Tweening;
using Gamelogic.Grids;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum ActorType
{
    None = 0,
    Cyborg = 1,


    Guard = 2,

    Explosive = 100,
}

public class Actor : ActorBase
{
    public Enemy enemy;

    public void Init()
    {
        enemy = new Enemy(this);
    }

    public static bool IsEnemy(ActorType type)
    {
        return type == ActorType.Guard;
    }

    public static bool IsPlayerUnit(ActorType type)
    {
        return type == ActorType.Cyborg;
    }

    internal static Color GetActorColor(ActorType type)
    {
        switch (type)
        {
            case ActorType.Guard:
                return Helpers.HexToColor("FC03F8");

            case ActorType.Cyborg:
                return Helpers.HexToColor("5C5C5C");
        }
        return Color.black;
    }

    internal static string GetSpriteName(ActorType type)
    {
        return "cyborg-face";
    }

    internal void HandleMoved()
    {
        if(this.IsPlayerUnit() && this.cell.terrainType == TerrainType.Gate)
        {
            this.cell.DestroyActor(DeathAnimation.CustomDeathAnim, Vector3.zero, false);
            this.CallActionDelayed(EscapeAnim, 0.3f);

            ResourceManager.Instance.Gain(ResourceType.Escaped, 1);
        }
    }

    public void EnteredBoard()
    {
        if (enemy != null)
            enemy.EnteredBoard();
    }

    public void CacheEnemyMoves()
    {
        if (enemy != null)
            enemy.CacheEnemyMoves();
    }

    public bool HasPreMove()
    {
        if (enemy != null)
            return false;

        return false;
    }

    public void DoPreMove(Action donePreMoveCallback)
    {
        if (enemy != null)
            enemy.DoPreMove(donePreMoveCallback);
        else
            donePreMoveCallback();
    }

    public void DoEnemyMove()
    {
        if (enemy != null)
            enemy.DoEnemyMove();
    }

    public List<RectCell> EnemyValidMoves()
    {
        if (enemy != null)
            return enemy.validMoves;

        return new List<RectCell>();
    }
}
