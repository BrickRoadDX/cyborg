﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using DG.Tweening;
using Gamelogic.Grids;
using System.Linq;
using TMPro;

public enum Player
{
    None = 0,
    You = 1,
    Opponent = 2,
}

public enum ComboType
{
    None = 0,
    Flush = 1,
    Straight = 2,
    XofAKind = 3,
}

public class CardManager : Singleton<CardManager>
{
    private Vector2 CASTING_CARD_POS = new Vector2(-14.5f, 10.5f);
    private Vector2 OPPONENT_CASTING_CARD_POS = new Vector2(0f, -6.5f);

    public const int MAX_HAND_SIZE = 12;

    public const int STARTING_HAND_SIZE = 5;
    public const float CARD_SPACING = 4f;

    public const float DEFAULT_HAND_Y_POS = 4f;
    public const float OPPONENT_DEFAULT_HAND_Y_POS = -4f;

    public const float CASTING_HAND_Y_POS = 0f;

    public CardObject castingCardObject;

    public Transform handContainer;
    public List<CardObject> handCardObjects;

    public List<Card> playerDeck = new List<Card>();
    public List<Card> discard = new List<Card>();

    public TextMeshPro deckText;
    public TextMeshPro discardText;
    public bool tabletMode = false;

    public Camera tk2dCamera;

    internal bool canRewindCard;

    public float soundDelay = 0f;

    float pressTime = 0f;

    public ObjectDisplay cardMarket;

    public List<CardPile> cardPiles = new List<CardPile>();

    public void Init()
    {
        this.StopDelayedActions();

        handCardObjects.ClearAndDestroyList();

        castingCardObject.DestroySelf();
        castingCardObject = null;

        discard.Clear();

        soundDelay = 0f;

        SetupPlayerDeck();

        DrawUpTo(3);
    }

    void SetupPlayerDeck()
    {
        for(int i = 0; i < 10; i++)
        {
            playerDeck.Add(new Card(Helpers.GetAllEnumTypes<CardType>(true).RandomElement()));
        }

        for (int i = 0; i < 20; i++)
        {
            AddNewCardToDiscard(new Card(Helpers.GetAllEnumTypes<CardType>(true).RandomElement()));
        }
    }

    public void ReshuffleDiscard()
    {
        playerDeck.AddRange(discard);
        discard.Clear();
        playerDeck.Shuffle();
        Debug.Log(string.Format("Reshuffled {0} cards  " + GetCardListString(playerDeck), playerDeck.Count));
    }

    string GetCardListString(List<Card> cards)
    {
        string cardString = "";
        foreach (Card c in cards)
            cardString += c.GetCardString() + ",";

        cardString = cardString.Trim(',');
        return cardString;
    }

    public void DrawUpTo(int numCards, Player player = Player.You)
    {
        List<CardObject> cardObjects = handCardObjects;

        while (cardObjects.Count < numCards && (playerDeck.Count > 0 || discard.Count > 0))
        {
            DrawCard();
        }
    }

    public void DrawCard(Card forceDraw = null)
    {
        Transform handTransform = handContainer;
        List<CardObject> cardObjects = handCardObjects;

        if (cardObjects.Count >= MAX_HAND_SIZE)
        {
            Helpers.ShowReminderText(Helpers.AddIcons("Overdrew!"));
            return;
        }

        if (forceDraw == null && playerDeck.Count < 1) //drawing card from deck
        {
            ReshuffleDiscard();
        }

        Card drawnCard = playerDeck[0];
        playerDeck.RemoveAt(0);

        CardObject co = CreateCard(handTransform, drawnCard);

        cardObjects.Add(co);

        co.SetColliderSize(true);

        UpdateCardDisplays();
    }

    internal bool IsUsingCard(CardObject cardObject)
    {
        return castingCardObject == cardObject;
    }

    public static CardObject CreateCard(Transform parent, Card card)
    {
        CardObject co = Helpers.CreateInstance<CardObject>("CardObject", parent);
        co.SetCard(card);
        return co;
    }

    internal void DiscardCard(CardObject cardObject)
    {
        if (cardObject == null)
            return;

        discard.Add(cardObject.card);

        if (castingCardObject == cardObject)
            castingCardObject = null;

        handCardObjects.Remove(cardObject);

        cardObject.CleanUp();

        handContainer.DOKill();
        handContainer.DOLocalMoveY(DEFAULT_HAND_Y_POS, 0.5f);

        UpdateCardDisplays();
    }

    public void AddNewCardToDiscard(Card card)
    {
        discard.Add(card);
        UpdateCardDisplays();
    }

    internal Card GetPlayCard(Player player)
    {
        if (castingCardObject != null)
            return castingCardObject.card;

        return null;
    }

    internal bool IsCastingCard()
    {
        return castingCardObject != null;
    }

    public List<float> GetHandPositions(int numCards, float cardSpacing)
    {
        List<float> handPositions = new List<float>();
        if(numCards % 2 != 0)
        {
            handPositions.Add(0f);
            int cardsOutFromMiddle = Mathf.FloorToInt((float)numCards / (float)2);
            for(int i = 0; i < cardsOutFromMiddle; i++)
            {
                float position = (i+1) * cardSpacing;
                handPositions.Insert(0, -position);
                handPositions.Add(+position);
            }
        }
        else
        {
            int cardsOutFromMiddle = Mathf.FloorToInt((float)numCards / (float)2);
            for (int i = 0; i < cardsOutFromMiddle; i++)
            {
                float position = (i+1) * cardSpacing;
                position -= cardSpacing / 2f;
                handPositions.Insert(0, -position);
                handPositions.Add(+position);
            }
        }
        return handPositions;
    }

    public void UpdateCardDisplays()
    {
        UpdateDeckDiscardText();
        UpdateCardPositions(Player.You);
        UpdateCardPositions(Player.Opponent);

        foreach (CardPile pile in cardPiles)
            pile.UpdateCardPile();
    }

    public void UpdateCardPositions(Player player)
    {
        List<CardObject> cardObjects = handCardObjects;

        int index = 0;
        int middle = cardObjects.Count / 2;

        List<float> handSpacing = GetHandPositions(cardObjects.Count, CARD_SPACING);

        int cardIndex = 0;
        foreach (CardObject co in cardObjects)
        {
            float posX = handSpacing[cardIndex];

            co.handXpos = posX;
            co.handYpos = 0f;

            co.TweenToPos();
            cardIndex++;
        }
    }


    Collider raycastCollider = null;
    CardObject hitCardObject = null;
    public CardObject hoveredCardObject;
    CardObject newHoveredCardObject;
    float middleXpos = 0f;
    void Update()
    {
        if (soundDelay > 0f)
            soundDelay -= Time.deltaTime;

        if (!Game.HasPrivateInstance() || Game.Instance.IsGameOver())
            return;

        if(Input.GetMouseButtonUp(1) && IsCastingCard())
        {
            UndoPlayedCard();
            return;
        }

        raycastCollider = Helpers.RaycastFromMouse(tk2dCamera);
        hitCardObject = null;
        if (raycastCollider != null)
            hitCardObject = raycastCollider.GetComponent<CardObject>();

        if (hitCardObject != null)
        {
            newHoveredCardObject = hitCardObject;

            if (!newHoveredCardObject.IsPlayersCard())
                return;

            if (newHoveredCardObject != null && hoveredCardObject != null && newHoveredCardObject != hoveredCardObject)
            {
                ResetHoveredCard();
            }

            if (hoveredCardObject != newHoveredCardObject)
            {
                if (soundDelay <= 0f)
                    SoundManager.Instance.PlaySound("bip");

                //Game.Instance.ShowHoveredCard(newHoveredCardObject.card.type, null);
            }

            hoveredCardObject = newHoveredCardObject;
            hoveredCardObject.SetVerticalPos(3f);
        }
        else if (hoveredCardObject != null)
        {
            ResetHoveredCard();
        }

        if (Input.GetMouseButtonUp(1) && hoveredCardObject != null)
        {
            DiscardHoveredCard();
            return;
        }

        if(tabletMode)
        {
            if (!Input.GetMouseButton(0) && hoveredCardObject != null)
            {
                middleXpos = hoveredCardObject.transform.localPosition.x;
                pressTime = 0f;
            }

            if (Input.GetMouseButton(0) && hoveredCardObject != null)
            {
                pressTime += Time.deltaTime;
                hoveredCardObject.transform.SetLocalPositionX(middleXpos + (pressTime * Helpers.RandomFloatFromRangeInclusive(-15f, 15f)));

                if (pressTime >= 1f)
                    DiscardHoveredCard();
            }
        }
    }

    public void DiscardHoveredCard()
    {
        pressTime = 0f;

        DiscardCard(hoveredCardObject);
    }

    public bool CanUndo()
    {
        return canRewindCard && Game.Instance.inputState == InputState.CastingCard;
    }

    public void UndoPlayedCard()
    {
        if (IsCastingCard() && CanUndo())
        {
            soundDelay = 0.3f;
            SoundManager.Instance.PlaySound("warning");
            handCardObjects.Add(castingCardObject);
            castingCardObject.SetColliderSize(true);
            castingCardObject.SetRotation(Direction.Down);
            castingCardObject = null;
            handContainer.DOKill();
            handContainer.DOLocalMoveY(DEFAULT_HAND_Y_POS, 0.5f);

            Game.Instance.UndidCard();
            UpdateCardDisplays();
        }
    }

    void ResetHoveredCard()
    {
        hoveredCardObject.SetVerticalPos(0f);
        hoveredCardObject = null;
    }

    void UpdateDeckDiscardText()
    {
        if(deckText != null)
        {
            deckText.text = playerDeck.Count.ToString();
            discardText.text = discard.Count.ToString();
        }
    }

    public void DiscardHand(Player player)
    {
        DiscardTo(0, player);
    }

    internal void DiscardTo(int numCards, Player player)
    {
        List<CardObject> cardObjects = handCardObjects;

        while (cardObjects.Count > numCards)
        {
            DiscardCard(cardObjects[Helpers.RandomIntFromRange(0, cardObjects.Count)]);
        }
    }

    public void PlayCard(CardObject co)
    {
        //Debug.Log("Start casting: " + co.card.GetCardString());

        castingCardObject = co;

        SoundManager.Instance.PlaySound("doo dee");

        co.SetColliderSize(false);

        co.TweenToPos(CASTING_CARD_POS);

        handContainer.DOKill();
        handContainer.DOLocalMoveY(CASTING_HAND_Y_POS, 0.5f);

        handCardObjects.Remove(co);

        canRewindCard = true;

        Game.Instance.GoToState(InputState.CastingCard);
    }

    internal void DoneCastingCard()
    {
        DiscardCard(castingCardObject);
    }

    public void TryClickCard(int cardNum)
    {
        if(handCardObjects.Count >= cardNum)
        {
            handCardObjects[cardNum - 1].HandleCardClicked();
        }
    }

    internal bool CardDisplayUp()
    {
        foreach (CardPile pile in cardPiles)
            if (pile.DisplayUp())
                return true;

        return false;
    }
}
