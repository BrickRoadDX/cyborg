﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Gamelogic.Grids;
using System.Linq;
using TMPro;

public enum CardType
{
    None = 0,

    Knight = 101,
    One = 1,
    Two = 2,
    Three = 3,
}

public class Card
{
    public string descText = "";
    public CardType cardType = CardType.None;

    public int cardId = 0;
    public static int ID_GEN = 0;

    public Card(CardType type)
    {
        descText = "default title";
        this.cardType = type;
        ID_GEN++;
        cardId = ID_GEN;
    }

    internal void SetupCardObject(CardObject cardObject)
    {
        cardObject.card = this;

        cardObject.titleText.text = cardType.ToString();

        cardObject.SetupCardPattern();
    }

    internal void UpdateCardObject(CardObject cardObject)
    {
        cardObject.mainText.text = Card.GetMiddleText(cardType);
    }

    public static Sprite GetBackgroundSprite(CardType type)
    {
        switch(type)
        {
            case CardType.Knight:
                return Helpers.GetSprite("cardfront");
        }

        return Helpers.GetSprite("cardfront");
    }

    public static Color GetBackgroundColor(CardType type)
    {
        switch (type)
        {
            case CardType.None:
                return Color.black;
            case CardType.Knight:
                return Color.white;
        }

        return Color.white;
    }

    public static void DrawCard()
    {
        CardManager.Instance.DrawCard();
    }


    internal string GetCardString()
    {
        return cardType.ToString();
    }

    public static string GetTitleText(CardType type)
    {
        switch (type)
        {
            case CardType.None:
                return "";
            case CardType.Knight:
                return "Concept";
        }

        return type.ToString();
    }

    internal static string GetMiddleText(CardType type)
    {
        switch(type)
        {
            case CardType.Knight:
                return "Draw 1 Card";
        }

        return type.ToString();
    }

    internal static int GetCost(CardType type)
    {
        return 1;
    }

    public CardPattern GetCardPattern()
    {
        return CardPattern.GetCardPatternForCardType(cardType);
    }
}
