﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CardObject : MonoBehaviour, SortingOrderObject
{
	public Card card;
	public TextMeshPro mainText;
    public TextMeshPro titleText;
    public TextMeshPro costText;

	public SpriteRenderer bgSprite;
    public Transform patternContainer;

	public tk2dUIItem uiItem;

	public Transform rotater;

    public BoxCollider cardCollider;

    [HideInInspector]
    public Direction direction = Direction.None;

    private bool extended = false;

    [HideInInspector]
    public float handYpos = 0f;

    [HideInInspector]
    public float handXpos = 0f;

    [HideInInspector]
    public List<Renderer> renderers = new List<Renderer>();

    List<int> sortingOrder = new List<int>();

    public SpriteRenderer hiddenSprite;
    bool hidden = false;

    void Awake()
    {
        cachedBoxColliderSize = cardCollider.size;

        foreach (Renderer r in GetComponentsInChildren<Renderer>())
            renderers.Add(r);

        sortingOrder.Clear();
        foreach (Renderer r in this.renderers)
            sortingOrder.Add(r.sortingOrder);
    }

    public void ResetSortingOrders()
    {
        int index = 0;
        foreach (Renderer r in this.renderers)
        {
            r.sortingOrder = sortingOrder[index];
            index++;
        }
    }

    string planetSpriteName = "";
	public void SetCard(Card card)
	{
        titleText.text = "";
        mainText.text = "";
        costText.text = "";

        patternContainer.gameObject.DestroyChildren();

        card.SetupCardObject(this);

        SetColliderSize(false);

        gameObject.name = "Card: " + card.cardId + " " + card.GetCardString();
    }

	void OnEnable()
	{
		uiItem.OnClick += HandleCardClicked;
	}

	void OnDisable()
	{
		uiItem.OnClick -= HandleCardClicked;
	}

	public void SetVerticalPos(float yPos)
	{
		handYpos = yPos;
		TweenToPos();
	}

    public bool IsUsingCard(Player player = Player.You)
    {
        return CardManager.Instance.IsUsingCard(this);
    }

    Tweener posTweener;
    public void TweenToPos(Vector3 pos)
    {
        if (posTweener != null)
            posTweener.Kill();

        posTweener = transform.DOLocalMove(pos, 0.3f);
    }

	public void TweenToPos()
	{
		if(IsUsingCard())
		{
			return;
		}

        TweenToPos(new Vector3(handXpos, handYpos, transform.localPosition.z));
	}

    public bool IsPlayersCard()
    {
        return CardManager.Instance.handCardObjects.Contains(this) || IsUsingCard();
    }

    public bool IsShopCard()
    {
        if (CardManager.Instance.cardMarket == null)
            return false;

        return CardManager.Instance.cardMarket.GetCastedList<CardObject>().Contains(this);
    }

    public void HandleCardClicked()
	{
		if (Helpers.GameActionsBlocked())
			return;

        if(IsPlayersCard())
        {
            if (Game.Instance.inputState == InputState.Playing && !IsUsingCard())
            {
                CardManager.Instance.PlayCard(this);
            }
        }
        else if(IsShopCard() && Game.Instance.inputState == InputState.Playing)//is shop card
        {
            if(Game.Instance.CanAfford(Player.You, this.card))
            {
                Game.Instance.BuyCard(Player.You, this);
            }
            else
            {
                this.gameObject.transform.DOGoto(5f);
                this.transform.DOShakePosition(0.3f);
            }
        }
    }
    
    string soundName = "";
	public void Rotate(bool right = true)
	{
		int dirNum = (int)direction;
		dirNum = right ? dirNum + 1 : dirNum - 1;

		if (dirNum == 5)
			dirNum = 1;

		if (dirNum == 0)
			dirNum = 4;

		SetRotation((Direction)dirNum);


        soundName = !right ? "WCShort5" : "WCShort6";

        SoundManager.Instance.PlaySound(soundName);
	}

    public float GetZRotation()
    {
        switch(direction)
        {
            case Direction.Up:
                return 180f;
            case Direction.Left:
                return 270f;
            case Direction.Right:
                return 90f;
            case Direction.Down:
                return 0f;
        }
        return 0f;
    }

    Tweener rotateTweener;
	public void SetRotation(Direction d)
	{
		direction = d;

        if (rotateTweener != null)
            rotateTweener.Kill();

        RotateMode mode = RotateMode.Fast;

		//Debug.Log(d.ToString());
		//rotater.localRotation = Quaternion.Euler(0f, 0f, 0f);
		rotateTweener = rotater.DORotate(new Vector3(0f, 0f, 90f), 0.3f, mode);

		switch (direction)
		{
			case Direction.Up:
				rotater.DORotate(new Vector3(0f, 0f, 180f), 0.3f, mode);
				break;
			case Direction.Left:
				rotater.DORotate(new Vector3(0f, 0f, 270f), 0.3f, mode);
				break;
			case Direction.Right:
				rotater.DORotate(new Vector3(0f, 0f, 90f), 0.3f, mode);
				break;
			case Direction.Down:
				rotater.DORotate(new Vector3(0f, 0f, 0f), 0.3f, mode);
				break;
		}
	}

    public void CleanUp()
	{
        if(this != null && this.gameObject != null)
        {
            Destroy(this.gameObject);
        }
	}

    public void UpdateDisplay()
    {
        card.UpdateCardObject(this);
    }

    internal void SetHidden(bool hidden)
    {
        this.hidden = hidden;
        hiddenSprite.gameObject.SetActive(hidden);
    }

    Vector3 cachedBoxColliderSize = Vector3.zero;
    internal void SetColliderSize(bool isExtended)
    {
        extended = isExtended;
        Vector3 sizeToUse = cachedBoxColliderSize;
        Vector3 centerToUse = new Vector3(0f, -3f, 0f);

        if (!isExtended)
        {
            centerToUse = Vector3.zero;
            sizeToUse.y = -6f;
        }

        cardCollider.center = centerToUse;
        cardCollider.size = sizeToUse;
    }

    internal void SetupCardPattern()
    {
        CardPattern pattern = CardPattern.GetCardPatternForCardType(card.cardType);

        foreach (PatternSpace ps in pattern.patternSpaces)
        {
            CreateRenderer(ps, CardPattern.GetEffectTypeSprite(ps.effectType), patternContainer);
            CreateRenderer(ps, "outline", patternContainer);
        }
    }

    public SpriteRendererGo CreateRenderer(PatternSpace ps, string spriteName, Transform parent)
    {
        float xPos = 0f;
        float yPos = 0f;

        SpriteRendererGo srg = Helpers.CreateInstance<SpriteRendererGo>("SpriteRendererGo", parent);
        srg.gameObject.SetLayerRecursively("UI");
        srg.spriteRenderer.sprite = Helpers.GetSprite(spriteName);
        xPos = ps.position.X * 1f + CardPattern.GetXOffset(card.cardType);
        yPos = ps.position.Y * 1f + CardPattern.GetYOffset(card.cardType);
        srg.transform.localPosition = new Vector3(xPos, yPos, 0f);
        srg.transform.localScale = Vector3.one * 0.2f;
        srg.spriteRenderer.sortingOrder = 50;
        srg.spriteRenderer.color = Color.black;
        srg.gameObject.name = srg.spriteRenderer.sprite.name;

        return srg;
    }
}
