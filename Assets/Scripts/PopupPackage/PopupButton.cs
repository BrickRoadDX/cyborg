﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PopupButton : MonoBehaviour
{
    public tk2dUIItem uiItem;
    public TextMeshPro text;
    public Action callback;

    public void OnEnable()
    {
        uiItem.OnClick += HandleClicked;
    }

    void HandleClicked()
    {
        if (callback != null)
            callback();

        PopupManager.Instance.DismissPopup();
    }
}
