﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Board : BoardBase
{
    public void Init()
    {
        ClearBoard();

        SetupBoard();

        BuildGridAnimation();
    }

    void SetupBoard()
    {
        List<RectCell> candidates = GetBorder();
        for(int i = 0; i < 3; i++)
        {
            candidates.RemoveAll(x => x.GetAdjacent().Any(y => y.terrainType != TerrainType.None));
            candidates.Shuffle();
            RectCell c = candidates.PopFirst();
            c.SetTerrainType(TerrainType.Gate);
        }

        List<ActorType> typesToSpawn = new List<ActorType>() { ActorType.Guard };

        foreach (ActorType type in typesToSpawn)
            SpawnAppropriateNumber(type);

        CacheEnemyMoves();

        SpawnPlayerUnits();

        CacheEnemyMoves();

        BoardSetupDone();
    }

    void BoardSetupDone()
    {
        foreach (Actor a in actorsOnBoard)
            a.EnteredBoard();
    }

    public void SpawnPlayerUnits()
    {
        List<RectCell> nonThreatenedCells = GetEmptyNoTerrainCells();

        nonThreatenedCells.RemoveAll(x => x.IsThreatened());

        nonThreatenedCells.Shuffle();

        for (int i = 0; i < 3; i++)
        {
            RectCell c = GetPlayerSpawnCells().First();
            if (nonThreatenedCells.Count > 0)
                c = nonThreatenedCells.PopFirst();

            c.SpawnActor(ActorType.Cyborg);
        }
    }

    public List<RectCell> GetPlayerSpawnCells()
    {
        List<RectCell> cells = GetEmptyCells();
        cells.RemoveAll(x => x.terrainType != TerrainType.None);
        cells.Shuffle();

        cells.Sort(new RectCellComparer());

        return cells;
    }

}
